# Naming convention

in order to improve collaboration for `ui-components`'s development, we have adopted a naming convention for class names in the App's code.

## Disclaimer

All React components should defined their own class name as a `className` property. Consequently, all components can be fetched in the DOM directly with the name of the component. This class name should be formatted in **Pascal Case**. Moreover, all class names in the component should also be in **Pascal Case**. For example, the component *MyComponent* should be rendered as `<div className='MyComponent' />`.

Moreover, this glossary intends to improve the collaboration between the developers and the Quality Assurance department. The CSS selector is used here in order to make this documentation more compact but we encourage you to use the [Xpath cheatsheet](https://devhints.io/xpath) if you want to use XPaths for your tests.

## Common

All the common components are used accross multiple use cases in a webapp.

| Components       | Types                                | Description                                          | CSS selector         | Example           |
|------------------|--------------------------------------|------------------------------------------------------|----------------------|-------------------|
| Button           | See [the button types][button-types] | Button to trigger actions                            | `.button-<type>`     | `.button-primary` |
| Icon             | See [the icon types][icon-types]     | Icon that represents an action                       | `.icon-<type>`       | `.icon-video`     |
| Spinner          |                                      | Spinner used to display loading time             | `.spinner-container` |                   |
| Spinner backdrop |                                      | Backdrop used to hide the content behind the spinner | `.spinner-backdrop`  |                   |
| Tag              |                                      | Tag is used to categorize some data                  | `.tag`               |                   |

[button-types]: https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=button
[icon-types]: https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=icon

### Menus

The menus can be used as a *dropdown* menu or as a way to order data.

| Components     | Description                                                                | CSS selector           |
|----------------|----------------------------------------------------------------------------|------------------------|
| Menu           | Button that *displays/hides* the menu                                      | `.menu-button`         |
| Menu addon     | Adds icons before or after the menu title                                  | `.menu-select-addon`   |
| Wrapper        | Wraps all the menus and helps to perform the collabsible statement         | `.menu-wrapper`        |
| Group          | Groups all menu items into *categories*                                    | `.menu-group`          |
| Group title    | Displays the group category                                                | `.menu-group-title`    |
| Group addon    | Adds icons before or after the group title                                 | `.menu-group-addon`    |
| Group children | Encapsulates all group children                                            | `.menu-group-children` |
| Menu item      | Displays a menu item with a simple item style (in most case it is a text)  | `.menu-item`           |
| Menu panel     | Displays a menu panel with a panel style (it is used for fields in a menu) | `.menu-panel`          |

### NavTab

The `NavTab` should be used to display a bar with tabs.

| Components  | Description                                      | CSS selector           |
|-------------|--------------------------------------------------|------------------------|
| Bar         | Bar of the navigation tabs                       | `.navtab-bar`          |
| ExtraButton | Button that triggers special action from the bar | `.navbar-extra-button` |
| Panel       | A panel that is displays when a tab is clicked   | `.navtab-panel`        |
| Tab         | A tab of the bar                                 | `.navtab-bar`          |

## Feedback

All the feedback components are used to display specific information to users.

| Components      | Description                                           | CSS selector          |
|-----------------|-------------------------------------------------------|-----------------------|
| Drawer          | Drawer component that displays extra menu on the side | `.drawer`             |
| Drawer body     | Displays the body of the drawer                       | `.drawer > .body`     |
| Drawer header   | Displays the header of the drawer                     | `.drawer > .header`   |
| Modal           | Modal component that displays dialog                  | `.modal`              |
| Modal header    | Displays the header of the modal                      | `.modal > .header`    |
| Modal content   | Displays the content of the modal                     | `.modal > .content`   |
| Modal footer    | Displays the footer of the modal                      | `.modal > .footer`    |
| Preview         | Displays a media preview                              | `.preview`            |
| Tooltip         | Displays a tip over an element                        | `.Tooltip`            |
| Wrapped content | Wrapped element by the tooltip                        | `.Tooltip > .wrapped` |
| Popover content | Element displayed as a tip                            | `.Tooltip > .popover` |

## Inputs

All the components `InputText`, `InputNumber` and `InputPassword` reuses the HTML `input` element.

| Components                   | Description                              | CSS selector            |
|------------------------------|------------------------------------------|-------------------------|
| Checkbox                     | Lets the user check data                 | `.Checkbox`             |
| FormGroup                    | Encapsulates forms in a group            | `.FormGroup`            |
| InputNumber                  | Lets the user unput a number             | `.InputNumber`          |
| InputNumber increment button | Lets the user increment the input value  | `.InputButtonIncrement` |
| InputNumber decrement button | Lets the user decrement the input value  | `.InputButtonDecrement` |
| InputPassword                | Lets the user input a password           | `.InputPassword`        |
| InputText                    | Lets the user input a text               | `.InputText`            |
| InputSelect                  | Lets the user select data                | `.InputSelect`          |
| Select                       | Lets the user selects data               | `.Select`               |
| Slider                       | Lets the user selects data by sliding    | `.Slider`               |
| Switch                       | Lets the user switches between 2 choices | `.Switch`               |

## Layout

The layout components are used to structure the other components.

| Components | Description                                       | CSS selector |
|------------|---------------------------------------------------|--------------|
| Cell       | A cell used to be displayed in a matrix           | `.cell`      |
| Backdrop   | Blurs the elements behind the backdrop            | `.backdrop`  |
| FlexBox    | Defined the [CSS flexbox as a component][flexbox] | `.flex-box`  |

[flexbox]: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout

## Shared

The shared components are components for specific use cases.

| Components      | Description                         | CSS selector        |
|-----------------|-------------------------------------|---------------------|
| ColumnHead      | A column header of a matrix         | `.column-head`      |
| ColumnSeparator | A column separator of a matrix      | `.column-separator` |
| ConnectionCell  | A cell that represents a connection | `.connection-cell`  |
| RowHead         | A row header of a matrix            | `.row-head`         |
| SceneCell       | A cell that represents a scene      | `.scene-cell`       |
| ShmdataCell     | A cell that represents a shmdata    | `.shmdata-cell`     |
