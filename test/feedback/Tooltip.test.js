/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Tooltip from '~/feedback/Tooltip'

describe('<Tooltip />', () => {
  const shallowTooltip = (props = {}) => {
    return shallow(
      <Tooltip {...props} />
    )
  }

  describe('constructor', () => {
    it('should render a Tooltip', () => {
      expect(shallowTooltip()).toBeDefined()
    })

    it('should change the side of the tooltip with the `side` property', () => {
      expect(shallowTooltip({ side: 'top' }).find('.content-top').length).toEqual(1)
    })

    it('should force the visibility of the tooltip with the `visible` property', () => {
      expect(shallowTooltip({ visible: true }).find('.content-visible').length).toEqual(1)
    })

    it('should have the hovering behaviour by default', () => {
      expect(shallowTooltip().find('.target-default').length).toEqual(1)
    })

    it('should break the hovering behaviour with the `visibleOnHover` property', () => {
      expect(shallowTooltip({ visibleOnHover: false }).find('.target-default').length).toEqual(0)
    })
  })
})
