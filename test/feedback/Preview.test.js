/* global jest describe beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Backdrop from '~/layout/Backdrop'
import Preview from '~/feedback/Preview'

describe('<Preview />', () => {
  beforeEach(() => {
    Preview.defaultProps.onDestroy = jest.fn()
  })

  it('render a Preview with default properties', () => {
    const $preview = shallow(<Preview />)

    expect($preview.find('.preview')).toHaveLength(1)

    const screenStyle = $preview.find('.screen').prop('style')
    expect(screenStyle).toHaveProperty('width', Preview.defaultProps.width)
    expect(screenStyle).toHaveProperty('height', Preview.defaultProps.height)

    const $img = $preview.find('img')
    expect($img.prop('src')).not.toBeDefined()
    expect($img.prop('alt')).toEqual(Preview.defaultProps.alt)
  })

  it('render a Preview with defined properties', () => {
    const testWidth = 200
    const testHeight = 200
    const testSrc = 'testSrc'
    const testAlt = 'testAlt'

    const clickTest = jest.fn()
    const onDestroyTest = () => clickTest()

    const $preview = shallow(
      <Preview
        width={testWidth}
        height={testHeight}
        src={testSrc}
        alt={testAlt}
        onBackdropClick={onDestroyTest}
      />
    )

    expect($preview.find('.preview')).toHaveLength(1)

    const screenStyle = $preview.find('.screen').prop('style')
    expect(screenStyle).toHaveProperty('width', testWidth)
    expect(screenStyle).toHaveProperty('height', testHeight)

    const $img = $preview.find('img')
    expect($img.prop('src')).toEqual(testSrc)
    expect($img.prop('alt')).toEqual(testAlt)

    const $backdrop = $preview.find(Backdrop)
    $backdrop.simulate('click')
    expect(clickTest).toHaveBeenCalled()
  })
})
