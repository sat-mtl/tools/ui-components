/* global jest beforeEach describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Icon from '~/common/Icon'
import ConnectionCell from '~/shared/ConnectionCell'

describe('<ConnectionCell />', () => {
  describe('rendering', () => {
    it('should render a ConnectionCell without any custom attributes', () => {
      const $box = shallow(<ConnectionCell />).dive()
      expect($box.hasClass('connection-cell')).toEqual(true)
      expect($box.hasClass('connected')).toEqual(false)
      expect($box.hasClass('armed')).toEqual(false)
      expect($box.hasClass('disabled')).toEqual(false)
    })

    it('should render a connected ConnectionCell', () => {
      const $box = shallow(<ConnectionCell connected />).dive()
      expect($box.hasClass('connection-cell')).toEqual(true)
      expect($box.hasClass('connected')).toEqual(true)
      expect($box.hasClass('armed')).toEqual(false)
      expect($box.hasClass('disabled')).toEqual(false)
    })

    it('should render an armed ConnectionCell', () => {
      const $box = shallow(<ConnectionCell armed />).dive()
      expect($box.hasClass('connection-cell')).toEqual(true)
      expect($box.hasClass('connected')).toEqual(false)
      expect($box.hasClass('armed')).toEqual(true)
      expect($box.hasClass('disabled')).toEqual(false)
    })

    it('should render a disabled ConnectionCell', () => {
      const $box = shallow(<ConnectionCell disabled />).dive()
      expect($box.hasClass('connection-cell')).toEqual(true)
      expect($box.hasClass('connected')).toEqual(false)
      expect($box.hasClass('armed')).toEqual(false)
      expect($box.hasClass('disabled')).toEqual(true)
    })
  })

  describe('renderConnection', () => {
    it('should render a `flux` Icon when it is armed', () => {
      const $cell = shallow(<ConnectionCell armed />)
      const $icon = $cell.find(Icon)
      expect($icon.prop('type')).toEqual('flux')
    })

    it('should render a `flux` Icon when it is not armed and hovered', () => {
      const $cell = shallow(<ConnectionCell />)

      $cell.simulate('mouseenter')
      const $flux = $cell.find(Icon)
      expect($flux.prop('type')).toEqual('flux')

      $cell.simulate('mouseleave')
      expect($cell.find(Icon).length).toEqual(0)
    })

    it('should render a `close` Icon when it is armed and hovered', () => {
      const $cell = shallow(<ConnectionCell armed />)

      $cell.simulate('mouseenter')
      const $close = $cell.find(Icon)
      expect($close.prop('type')).toEqual('close')

      $cell.simulate('mouseleave')
      const $flux = $cell.find(Icon)
      expect($flux.prop('type')).toEqual('flux')
    })
  })

  describe('onClick', () => {
    let onClick, onClickMock

    beforeEach(() => {
      onClickMock = jest.fn()
      onClick = (_, arg2, arg3) => onClickMock(arg2, arg3)
    })

    it('should not return status if it is clicked when it is not connected or armed or disabled', () => {
      const $box = shallow(<ConnectionCell onClick={onClick} />)
      $box.simulate('click')
      expect(onClickMock).toHaveBeenCalledWith(false, false)
    })

    it('should return connected status if it is clicked while it is connected', () => {
      const $box = shallow(<ConnectionCell connected onClick={onClick} />)
      $box.simulate('click')
      expect(onClickMock).toHaveBeenCalledWith(false, true)
    })

    it('should return armed status if it is clicked while it is armed', () => {
      const $box = shallow(<ConnectionCell armed onClick={onClick} />)
      $box.simulate('click')
      expect(onClickMock).toHaveBeenCalledWith(true, false)
    })

    it('should not have click reaction if it is disabled', () => {
      const $box = shallow(<ConnectionCell disabled onClick={onClick} />).dive()
      $box.simulate('click')
      expect(onClickMock).not.toHaveBeenCalled()
    })
  })
})
