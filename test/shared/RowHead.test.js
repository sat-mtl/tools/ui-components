/* global jest beforeEach describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import RowHead from '~/shared/RowHead'
import StatusEnum from '~/common/StatusEnum'

describe('<RowHead />', () => {
  const NAME_TEST = 'NAME'
  const ID_TEST = 'ID'

  const shallowRowHead = ({
    name = NAME_TEST,
    quiddityId = ID_TEST,
    status = undefined,
    selected = false,
    disabled = false,
    onClick = Function.prototype,
    onMouseEnter = Function.prototype,
    onMouseLeave = Function.prototype
  } = {}) => {
    const $head = shallow(
      <RowHead
        title={name}
        subtitle={quiddityId}
        status={status}
        selected={selected}
        disabled={disabled}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      />
    )

    const $status = $head.find('.status')
    const $name = $head.find('.title')
    const $id = $head.find('.subtitle')

    return { $head, $status, $name, $id }
  }

  describe('rendering', () => {
    it('should render a RowHead without any custom attributes', () => {
      const { $head, $status, $name, $id } = shallowRowHead()
      expect($head.hasClass('row-head')).toEqual(true)

      expect($status.hasClass('started')).toEqual(false)
      expect($status.hasClass('disabled')).toEqual(false)

      expect($name.text()).toEqual(NAME_TEST)
      expect($id.text()).toEqual(ID_TEST)
    })

    it('should render a RowHead with a status', () => {
      Object.values(StatusEnum).forEach(status => {
        const { $status } = shallowRowHead({ status: status })
        expect($status.hasClass(new RegExp(status))).toEqual(true)
      })
    })

    it('should render a disabled RowHead', () => {
      const { $head } = shallowRowHead({ disabled: true })
      expect($head.hasClass('disabled')).toEqual(true)
    })

    it('should render a selected RowHead', () => {
      const { $head } = shallowRowHead({ selected: true })
      expect($head.hasClass('selected')).toEqual(true)
    })
  })

  describe('onClick', () => {
    let onClickMock

    beforeEach(() => {
      onClickMock = jest.fn()
    })

    it('should trigger `onclick` event when user clicks on a head', () => {
      const { $head } = shallowRowHead({ onClick: onClickMock })
      $head.simulate('click')
      expect(onClickMock).toHaveBeenCalled()
    })

    it('shouldn\'t trigger `onclick` event when user clicks on a disabled head', () => {
      const { $head } = shallowRowHead({ disabled: true, onClick: onClickMock })
      $head.simulate('click')
      expect(onClickMock).not.toHaveBeenCalled()
    })
  })
})
