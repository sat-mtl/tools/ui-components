/* global jest beforeEach describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import ColumnHead from '~/shared/ColumnHead'
import StatusEnum from '~/common/StatusEnum'

describe('<ColumnHead />', () => {
  const NAME_TEST = 'NAME'
  const ID_TEST = 'ID'

  const shallowColumnHead = ({
    name = NAME_TEST,
    quiddityId = ID_TEST,
    status = undefined,
    selected = false,
    disabled = false,
    onClick = Function.prototype,
    onMouseEnter = Function.prototype,
    onMouseLeave = Function.prototype
  } = {}) => {
    const $head = shallow(
      <ColumnHead
        title={name}
        subtitle={quiddityId}
        status={status}
        selected={selected}
        disabled={disabled}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      />
    )

    const $top = $head.find('.top')
    const $bottom = $head.find('.bottom')
    const $status = $head.find('.status')
    const $name = $head.find('.title')
    const $id = $head.find('.subtitle')

    return { $head, $top, $bottom, $status, $name, $id }
  }

  describe('rendering', () => {
    it('should render a ColumnHead without any custom attributes', () => {
      const { $head, $top, $bottom, $status, $name, $id } = shallowColumnHead()
      expect($head.hasClass('column-head')).toEqual(true)

      expect($top.hasClass('selected')).toEqual(false)
      expect($top.hasClass('disabled')).toEqual(false)

      expect($bottom.hasClass('selected')).toEqual(false)
      expect($bottom.hasClass('disabled')).toEqual(false)

      expect($status.hasClass('started')).toEqual(false)
      expect($status.hasClass('disabled')).toEqual(false)

      expect($name.text()).toEqual(NAME_TEST)
      expect($id.text()).toEqual(ID_TEST)
    })

    it('should render a ColumnHead with a status', () => {
      Object.values(StatusEnum).forEach(status => {
        const { $status } = shallowColumnHead({ status: status })
        expect($status.hasClass(new RegExp(status))).toEqual(true)
      })
    })

    it('should render a disabled ColumnHead', () => {
      const { $head, $top, $bottom, $status } = shallowColumnHead({ disabled: true })
      expect($head.hasClass('disabled')).toEqual(true)
      expect($top.hasClass('disabled')).toEqual(true)
      expect($bottom.hasClass('disabled')).toEqual(true)
      expect($status.hasClass('disabled')).toEqual(true)
    })

    it('should render a selected ColumnHead', () => {
      const { $head, $top, $bottom } = shallowColumnHead({ selected: true })
      expect($head.hasClass('selected')).toEqual(true)
      expect($top.hasClass('selected')).toEqual(true)
      expect($bottom.hasClass('selected')).toEqual(true)
    })
  })

  describe('onClick', () => {
    let onClickMock

    beforeEach(() => {
      onClickMock = jest.fn()
    })

    it('should trigger `onclick` event when user click on a head', () => {
      const { $top } = shallowColumnHead({ onClick: onClickMock })
      $top.simulate('click')
      expect(onClickMock).toHaveBeenCalled()
    })

    it('shouldn\'t trigger `onclick` event when user click on a disabled head', () => {
      const { $top } = shallowColumnHead({ disabled: true, onClick: onClickMock })
      $top.simulate('click')
      expect(onClickMock).not.toHaveBeenCalled()
    })
  })
})
