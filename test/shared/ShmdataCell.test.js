/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import ShmdataCell from '~/shared/ShmdataCell'

import Icon from '~/common/Icon'
import Cell from '~/layout/Cell'

describe('<ShmdataCell />', () => {
  const shallowShmdataCell = ({ onClick, selected = false, disabled = false, icon, src, alt = '' } = {}) => {
    const $cell = shallow(
      <ShmdataCell selected={selected} disabled={disabled} onClick={onClick} icon={icon} src={src} alt={alt} />
    ).dive()

    const $icon = $cell.find(Icon)
    const $thumb = $cell.find('img')

    return { $icon, $cell, $thumb }
  }

  describe('rendering', () => {
    it('should render a ShmdataCell without any custom attributes', () => {
      const { $icon, $cell, $thumb } = shallowShmdataCell()

      expect($cell.hasClass('shmdata-cell')).toEqual(true)
      expect($icon.length).toEqual(0)
      expect($thumb.length).toEqual(0)
    })

    it('should render a ShmdataCell with an icon', () => {
      const $cell = shallow(<ShmdataCell icon={<Icon type='audio' />} />)
      const $icon = $cell.find(Icon)
      const $thumb = $cell.find('img')
      expect($icon.length).toEqual(1)
      expect($thumb.length).toEqual(0)
    })

    it('should alter padding if the icon has a fixed padding', () => {
      const $shm = shallow(<ShmdataCell icon={<Icon type='data' />} />)
      const $cell = $shm.find(Cell)
      expect($cell.prop('padding')).toEqual(5)
    })

    it('should render a ShmdataCell with a thumbnail', () => {
      const { $icon, $thumb } = shallowShmdataCell({ src: 'test' })
      expect($icon.length).toEqual(0)
      expect($thumb.length).toEqual(1)
    })

    it('shouldn\'t render an icon if a thumbnail is enabled', () => {
      const { $icon, $thumb } = shallowShmdataCell({ src: 'test', icon: <div /> })
      expect($icon.length).toEqual(0)
      expect($thumb.length).toEqual(1)
    })

    it('shouldn\'t render an icon or thumbnail if it is disabled', () => {
      const { $icon, $thumb } = shallowShmdataCell({ disabled: true, src: 'test', icon: <div /> })
      expect($icon.length).toEqual(0)
      expect($thumb.length).toEqual(0)
    })
  })

  describe('onClick', () => {
    it('should trigger `onShmdataClick`', () => {
      const onClickMock = jest.fn()
      const { $cell } = shallowShmdataCell({ onClick: onClickMock })
      $cell.simulate('click')
      expect(onClickMock).toHaveBeenCalled()
    })

    it('shouldn\'t trigger `onShmdataClick` when it is disabled', () => {
      const onClickMock = jest.fn()
      const { $cell } = shallowShmdataCell({ disabled: true, onClick: onClickMock })
      $cell.simulate('click')
      expect(onClickMock).not.toHaveBeenCalled()
    })
  })
})
