/* global describe it expect jest */

import React from 'react'
import { shallow } from 'enzyme'

import Cell, { renderColor } from '~/layout/Cell/Cell.js'

describe('<Cell />', () => {
  describe('renderColor', () => {
    it('should render a color if it is defined', () => {
      expect(renderColor({ color: 'red' })).toEqual('red')
    })

    it('should return null if the color is not defined', () => {
      expect(renderColor({})).toEqual(null)
    })
  })

  describe('render', () => {
    it('should trigger onClick if it is not disabled and onClick is defined', () => {
      const onClick = jest.fn()
      const $cell = shallow(<Cell onClick={onClick} />)
      $cell.simulate('click')
      expect(onClick).toHaveBeenCalled()
    })

    it('should not trigger onClick if it is disabled', () => {
      const onClick = jest.fn()
      const $cell = shallow(<Cell disabled onClick={onClick} />)
      $cell.simulate('click')
      expect(onClick).not.toHaveBeenCalled()
    })
  })
})
