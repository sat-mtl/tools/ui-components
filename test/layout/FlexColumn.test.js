/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import FlexColumn from '~/layout/FlexColumn'
import FlexBox from '~/layout/FlexBox'

describe('<FlexColumn />', () => {
  describe('rendering', () => {
    it('should render a FlexColumn from a Flexbox with a column direction', () => {
      const $flex = shallow(<FlexColumn />)
      expect($flex).toBeDefined()

      const $box = $flex.find(FlexBox)
      expect($box.length).toEqual(1)
      expect($box.prop('flexDirection')).toEqual('column')
    })
  })
})
