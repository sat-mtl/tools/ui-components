/* global describe it expect */

import React from 'react'
import { shallow, mount } from 'enzyme'

import { Button, Wrapper, MenuItem } from 'react-aria-menubutton'
import ScrollBar from 'react-customscroll'

import Select from '~/inputs/Select/Select.js'
import SelectMenu, { Option } from '~/inputs/Select/SelectMenu.js'

const TEST_LABEL = 'test'

const OPT1 = { label: TEST_LABEL, value: TEST_LABEL }
const OPT2 = { label: TEST_LABEL, value: TEST_LABEL }

describe('<Select />', () => {
  describe('enabled', () => {
    it('render a Select input from react-aria-menubutton without item', () => {
      const $select = shallow(<Select />)
      expect($select.find(Button)).toHaveLength(1)
      expect($select.find(Wrapper)).toHaveLength(1)
      expect($select.find(SelectMenu)).toHaveLength(1)
    })

    it('render a Select input with a placeholder', () => {
      const $select = mount(<Select placeholder={TEST_LABEL} />)
      const $placeholder = $select.find('.SelectField')

      expect($placeholder.text()).toEqual(TEST_LABEL)
    })

    it('render a Select input with two items', () => {
      const $select = shallow(<Select options={[OPT1, OPT2]} />)
      const $menu = $select.find(SelectMenu)

      $menu.find(Option).forEach($option => {
        expect($option.render().text()).toEqual(TEST_LABEL)
      })
    })

    it('render the first selected object as label', () => {
      const $select = mount(<Select selected={{ label: 'test' }} />)
      expect($select.find('.SelectField').text()).toEqual('test')
    })
  })

  describe('disabled', () => {
    it('render a disabled Select', () => {
      const $select = mount(<Select disabled placeholder={TEST_LABEL} />)

      expect($select.find(Wrapper).hasClass('Select-disabled')).toEqual(true)
      expect($select.find(Button).prop('disabled')).toEqual(true)
    })
  })
})

describe('<SelectMenu />', () => {
  describe('render', () => {
    it('should render a scrollbar', () => {
      const $menu = shallow(<SelectMenu options={[]} />)
      expect($menu.find(ScrollBar).length).toEqual(1)
    })

    it('should render a scrollbar', () => {
      const $menu = shallow(<SelectMenu options={[OPT1, OPT2]} />)
      expect($menu.find(Option).length).toEqual(2)
    })
  })
})

describe('<Option />', () => {
  it('should render a MenuItem', () => {
    const $opt = shallow(<Option {...OPT1} />)
    expect($opt.find(MenuItem).length).toEqual(1)
  })
})
