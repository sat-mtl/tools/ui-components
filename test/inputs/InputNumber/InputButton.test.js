/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import InputButton from '~/inputs/InputNumber/InputButton.js'

import Icon from '~/common/Icon'
import Button from '~/common/Button'

describe('<InputButton />', () => {
  const CLASS_NAME = 'test'
  const TYPE = 'info'

  describe('render', () => {
    it('should render an InputButton with defined props', () => {
      const $input = shallow(<InputButton className={CLASS_NAME} type={TYPE} />)
      expect($input.find(Icon)).toHaveLength(1)
      expect($input.find(Button)).toHaveLength(1)
      expect($input.hasClass(CLASS_NAME)).toEqual(true)
    })

    it('should render a primary button by default', () => {
      const $input = shallow(<InputButton />)
      const $btn = $input.find(Button)
      expect($btn.prop('type')).toEqual('primary')
    })

    it('should render an InputButton with errors', () => {
      const $input = shallow(<InputButton status='danger' />)
      const $btn = $input.find(Button)
      expect($btn.prop('type')).toEqual('danger')
    })
  })

  describe('onClick', () => {
    it('should call `onClick` prop if user clicked on it', () => {
      const onClickMock = jest.fn()
      const $input = shallow(<InputButton onClick={onClickMock} />)
      const $btn = $input.find(Button)
      $btn.simulate('click')
      expect(onClickMock).toHaveBeenCalled()
    })
  })
})
