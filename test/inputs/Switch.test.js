/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Switch from '~/inputs/Switch/Switch.js'

describe('<Switch />', () => {
  const getLabel = ($switch) => $switch.find('.SwitchLabel')

  describe('render', () => {
    it('should render an Input.Switch', () => {
      const $input = shallow(<Switch />)
      expect($input.hasClass('Switch-unchecked')).toEqual(true)
      expect($input.hasClass('Switch-checked')).toEqual(false)
    })

    it('should render a disabled Input.Switch', () => {
      const $input = shallow(<Switch disabled />)
      expect($input.find('button')).toHaveLength(1)

      const $btn = $input.find('button')
      expect($input.hasClass('Switch-disabled')).toEqual(true)
      expect($btn.prop('disabled')).toEqual(true)
    })

    it('should render a label for the `checked` status', () => {
      const $checked = shallow(<Switch checked />)
      expect(getLabel($checked).text()).toEqual('ON')
    })

    it('should render a label for `unchecked` status', () => {
      const $unchecked = shallow(<Switch />)
      expect(getLabel($unchecked).text()).toEqual('OFF')
    })
  })

  describe('onChange', () => {
    const getButton = ($switch) => $switch.find('.SwitchButton')

    it('should call `onChange` handler when user clicks', () => {
      const mockChange = jest.fn()
      const $input = shallow(<Switch onChange={mockChange} />)
      getButton($input).simulate('click', {})
      expect(mockChange).toHaveBeenCalledWith(true, {})
    })

    it('should not call `onChange` handler when it is disabled', () => {
      const mockChange = jest.fn()
      const $input = shallow(<Switch onChange={mockChange} disabled />)
      getButton($input).simulate('click', {})
      expect(mockChange).not.toHaveBeenCalledWith(true, {})
    })
  })
})
