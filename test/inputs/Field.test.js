/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Field from '~/inputs/Field'

describe('<Field />', () => {
  it('should render a Field with a title', () => {
    const $field = shallow(<Field title='test' />)
    expect($field.find('.FieldTitle').text()).toEqual('test')
  })

  it('should render a Field with a description', () => {
    const $field = shallow(<Field description='test' />)
    expect($field.find('.FieldDescription').text()).toEqual('test')
  })

  it('should render a Field without a description and a title', () => {
    const $form = shallow(<Field />)
    expect($form.find('.FieldTitle').length).toEqual(0)
    expect($form.find('.FieldDescription').length).toEqual(0)
  })
})
