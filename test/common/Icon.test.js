/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Icon from '~/common/Icon'

describe('<Icon />', () => {
  describe('rendering', () => {
    it('should render an Icon', () => {
      const $icon = shallow(<Icon type='audio' />)
      expect($icon).toBeDefined()
    })
  })
})
