/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Tag from '~/common/Tag'
import StatusEnum from '~/common/StatusEnum'

describe('<Tag />', () => {
  it('should render an Tag', () => {
    expect(shallow(<Tag>tag</Tag>)).toBeDefined()
  })

  it('should not have color and status by default', () => {
    const $tag = shallow(<Tag>tag</Tag>)
    expect($tag.hasClass(/status/)).toEqual(false)
    expect($tag.hasClass(/color/)).toEqual(false)
  })

  it('should have status class if a `status` prop is set', () => {
    Object.values(StatusEnum).forEach(status => {
      const $tag = <Tag status={status}>tag</Tag>
      const regex = new RegExp(status)
      expect(shallow($tag).hasClass(regex)).toEqual(true)
    })
  })

  it('should have jss classes if a `color` prop is set', () => {
    const $tag = <Tag color='#fff'>tag</Tag>
    expect(shallow($tag).hasClass(/color/)).toEqual(true)
  })

  it('should have css classes if a `onClick` prop is set', () => {
    const $tag = <Tag onClick={() => {}}>tag</Tag>
    expect(shallow($tag).hasClass(/clickable/)).toEqual(true)
  })

  const $addon = 'test'

  it('should set an addon before the label', () => {
    const $tag = <Tag addonBefore={$addon}>tag</Tag>
    expect(shallow($tag).find('.addon-before').text()).toEqual($addon)
  })

  it('should set an addon after the label', () => {
    const $tag = <Tag addonAfter={$addon}>tag</Tag>
    expect(shallow($tag).find('.addon-after').text()).toEqual($addon)
  })

  it('should trigger the `onClick` handler if it is clicked', () => {
    const onClickMock = jest.fn()
    const $tag = <Tag onClick={onClickMock}>tag</Tag>
    shallow($tag).simulate('click')
    expect(onClickMock).toHaveBeenCalled()
  })
})
