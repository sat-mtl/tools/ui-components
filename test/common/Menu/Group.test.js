/* global jest describe beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import { mockedEvent, mockEventFunction } from '~test/mock-component'
import { defaultTheme } from '~/contexts/Theme'
import Menu from '~/common/Menu'

describe('<Menu.Group />', () => {
  const emptyString = ''
  const classRoot = 'Group'

  beforeEach(() => {
    const { defaultProps } = Menu.Group

    defaultProps.onClick = jest.fn()

    const onMouseDown = defaultProps.onMouseDown
    defaultProps.onMouseDown = mockEventFunction(onMouseDown)
  })

  const shallowGroupMenu = ({ title, showGroup, collapsible, accordion, onClick, onMouseDown, dataMenu }) => {
    const $group = shallow(
      <Menu.Group
        title={title}
        showGroup={showGroup}
        accordion={accordion}
        collapsible={collapsible}
        onClick={onClick}
        onMouseDown={onMouseDown}
        dataMenu={dataMenu}
      />
    )

    const $root = $group.find('.Group')
    const $child = $group.find('.GroupChildren')
    const $title = $group.find('.GroupTitle')

    return { $group, $root, $child, $title }
  }

  describe('rendering', () => {
    it('render Menu.Group with default properties', () => {
      const { $root, $child, $title } = shallowGroupMenu({})

      expect($root.hasClass(classRoot)).toEqual(true)
      expect($root.hasClass(`Collapsible${classRoot}`)).toEqual(false)
      expect($root.hasClass(`Accordion${classRoot}`)).toEqual(false)
      expect($child.hasClass('CollapsedGroup')).toEqual(true)
      expect($child.find(Menu.Wrapper)).toHaveLength(1)
      expect($title.text()).toEqual(emptyString)
    })

    it('render Menu.Group with `collapsed` properties', () => {
      const { $root, $child } = shallowGroupMenu({ showGroup: true, collapsible: true })

      expect($root.hasClass(`Collapsible${classRoot}`)).toEqual(true)
      expect($child.hasClass(`Collapsed${classRoot}`)).toEqual(false)
    })

    it('render Menu.Group with `accordion` properties', () => {
      const { $root, $child } = shallowGroupMenu({ showGroup: true, accordion: true })

      expect($root.hasClass(`Collapsible${classRoot}`)).toEqual(true)
      expect($root.hasClass(`Accordion${classRoot}`)).toEqual(true)
      expect($child.hasClass(`Collapsed${classRoot}`)).toEqual(false)
    })

    it('render Menu.Group with a title', () => {
      const testTitle = 'test'
      const { $title } = shallowGroupMenu({ title: testTitle })

      expect($title.text()).toEqual(testTitle)
    })

    it('should bind a data-menu attributes to the dataMenu property of the group', () => {
      const onClickSpy = jest.fn()
      const { $root } = shallowGroupMenu({ dataMenu: 'Test', onClick: onClickSpy })
      const $title = $root.find('[data-menu="Test"]')
      $title.simulate('click')
      expect(onClickSpy).toHaveBeenCalled()
    })
  })

  describe('theme', () => {
    it('render Menu.Group with the default theme', () => {
      const { $root } = shallowGroupMenu({})

      expect($root.hasClass(`Group-${defaultTheme}`)).toEqual(true)
    })
  })

  describe('events', () => {
    it('simulate default `onClick` event on Menu.Group', () => {
      const { $title } = shallowGroupMenu({})

      $title.simulate('click')
      expect(Menu.Group.defaultProps.onClick).toHaveBeenCalled()
    })

    it('simulate default `onMouseDown` event onMenu.Group', () => {
      const { $title } = shallowGroupMenu({})

      $title.simulate('mousedown')
      expect(mockedEvent.stopPropagation).toHaveBeenCalled()
      expect(mockedEvent.preventDefault).toHaveBeenCalled()
    })

    it('simulate `onClick` event onMenu.Group', () => {
      const clickTrigger = jest.fn()
      const { $title } = shallowGroupMenu({ onClick: clickTrigger })

      $title.simulate('click')
      expect(clickTrigger).toHaveBeenCalled()
    })

    it('simulate `onMouseDown` event onMenu.Group', () => {
      const mouseDownTrigger = jest.fn()
      const { $title } = shallowGroupMenu({ onMouseDown: mouseDownTrigger })

      $title.simulate('mousedown')
      expect(mouseDownTrigger).toHaveBeenCalled()
    })
  })
})
