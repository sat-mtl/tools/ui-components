/* global jest describe beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import { defaultTheme } from '~/contexts/Theme'
import Menu from '~/common/Menu'

describe('<Menu.Wrapper />', () => {
  beforeEach(() => {
    const { defaultProps } = Menu.Wrapper
    defaultProps.onBlur = jest.fn()
  })

  const shallowWrapperMenu = ({ onBlur, menuRef, top, width }) => {
    const $item = shallow(
      <Menu.Wrapper onBlur={onBlur} menuRef={menuRef} top={top} width={width} />
    )

    const $ul = $item.find('.Wrapper')
    const style = $ul.prop('style')

    return { $item, $ul, style }
  }

  describe('rendering', () => {
    const topClass = `Wrapper-${defaultTheme}-top`

    it('render Menu.Wrapper with default properties', () => {
      const { $ul, style } = shallowWrapperMenu({})

      expect($ul.hasClass(topClass)).toEqual(false)
      expect($ul.prop('ref')).not.toBeDefined()
      expect(style).toHaveProperty('width', Menu.Wrapper.defaultProps.width)
    })

    it('render Menu.Wrapper with `top` property', () => {
      const { $ul } = shallowWrapperMenu({ top: true })

      expect($ul.hasClass(topClass)).toEqual(true)
    })

    it('render Menu.Wrapper with defined properties', () => {
      const width = 100
      const { $ul, style } = shallowWrapperMenu({ width: width })

      expect($ul.hasClass(topClass)).toEqual(false)
      expect(style).toHaveProperty('width', width)
    })
  })

  describe('events', () => {
    it('simulate default `onBlur` event on Menu.Item', () => {
      const { $ul } = shallowWrapperMenu({})

      $ul.simulate('blur')
      expect(Menu.Wrapper.defaultProps.onBlur).toHaveBeenCalled()
    })
  })
})
