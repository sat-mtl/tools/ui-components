/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'
import Menu from '~/common/Menu'

describe('<Menu.Item />', () => {
  const emptyString = ''

  const shallowItemMenu = ({ title, onClick, onMouseDown, dataMenu }) => {
    return shallow(
      <Menu.Item onClick={onClick} onMouseDown={onMouseDown} dataMenu={dataMenu}>
        {title}
      </Menu.Item>
    )
  }

  describe('rendering', () => {
    it('render Menu.Item with default properties', () => {
      const $li = shallowItemMenu({})

      expect($li.text()).toEqual(emptyString)
    })

    it('render Menu.Item with a title', () => {
      const testTitle = 'test'
      const $li = shallowItemMenu({ title: testTitle })

      expect($li.text()).toEqual(testTitle)
    })
  })

  describe('dataMenu', () => {
    it('should binds the dataMenu property as a data attribute', () => {
      const $li = shallowItemMenu({ dataMenu: 'Test' })
      expect($li.render().data('menu')).toEqual('Test')
    })
  })

  describe('events', () => {
    it('simulate default `onClick` event on Menu.Item', () => {
      const onClickSpy = jest.fn()
      const $li = shallowItemMenu({ onClick: onClickSpy })

      $li.simulate('click')
      expect(onClickSpy).toHaveBeenCalled()
    })

    it('simulate default `onMouseDown` event on Menu.Item', () => {
      const $li = shallowItemMenu({ onClick: jest.fn() })
      const stopPropagationMock = jest.fn()
      const preventDefaultMock = jest.fn()

      $li.simulate('mousedown', {
        stopPropagation: stopPropagationMock,
        preventDefault: preventDefaultMock
      })

      expect(stopPropagationMock).toHaveBeenCalled()
      expect(preventDefaultMock).toHaveBeenCalled()
    })
  })
})
