/* global describe it jest expect */

import React from 'react'
import { shallow } from 'enzyme'

import Button, { types, shapes } from '~/common/Button/Button.js'

describe('<Button />', () => {
  describe('enabled', () => {
    it('should render a Button with default props', () => {
      const $btn = shallow(<Button />)
      expect($btn).toBeDefined()
      expect($btn.hasClass('Button-primary')).toEqual(true)
    })

    it('should render a Button with all kind of types', () => {
      types.forEach(type => {
        const $btn = shallow(<Button type={type} />)
        expect($btn.hasClass(`Button-${type}`)).toEqual(true)
      })
    })

    it('should render a Button with all kind of shapes', () => {
      shapes.forEach(shape => {
        const $btn = shallow(<Button shape={shape} />)
        expect($btn.hasClass(`Button-${shape}`)).toEqual(true)
      })
    })

    it('should trigger onClick when user clicks on the Button', () => {
      const mockClick = jest.fn()
      const $btn = shallow(<Button onClick={mockClick} />)

      $btn.simulate('click')
      expect(mockClick).toHaveBeenCalled()
    })
  })

  describe('disabled', () => {
    it('should render a disabled Button', () => {
      const $btn = shallow(<Button disabled />)
      expect($btn.hasClass('Button-disabled')).toEqual(true)
    })

    it('should not trigger onClick when user clicks on a disabled Button', () => {
      const mockClick = jest.fn()
      const $btn = shallow(<Button disabled onClick={mockClick} />)

      $btn.simulate('click')
      expect(mockClick).not.toHaveBeenCalled()
    })
  })
})
