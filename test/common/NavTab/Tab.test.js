/* global describe it expect */

import React from 'react'
import NavTab from '~/common/NavTab'
import { shallow } from 'enzyme'

describe('<NavTab.Tab />', () => {
  const emptyString = ''

  const shallowTab = ({ onClick, addonBefore, addonAfter, selected, disabled, className, style, title }) => {
    const $tab = shallow(
      <NavTab.Tab
        onClick={onClick}
        addonBefore={addonBefore}
        addonAfter={addonAfter}
        selected={selected}
        disabled={disabled}
        className={className}
        style={style}
      >{title}
      </NavTab.Tab>
    )

    const $title = $tab.find('.navtab-tab-title')
    const $addons = $tab.find('.navtab-tab-addon')

    return { $tab, $title, $addons }
  }

  describe('rendering', () => {
    it('render NavTab.Tab with default properties', () => {
      const { $tab, $title, $addons } = shallowTab({})

      expect($tab.find('li').length).toEqual(1)
      expect($title.text()).toEqual(emptyString)
      expect($addons.length).toEqual(0)
    })

    it('render NavBar.Tab with addons', () => {
      const { $addons } = shallowTab({
        addonBefore: <div className='addon-before' />,
        addonAfter: <div className='addon-after' />
      })

      expect($addons.length).toEqual(2)
      expect($addons.find('.addon-before').length).toEqual(1)
      expect($addons.find('.addon-after').length).toEqual(1)
    })

    it('render selected NavBar.Tab', () => {
      const { $tab } = shallowTab({ selected: true })

      expect($tab.find('.navtab-tab-selected').length).toEqual(1)
    })

    it('render disabled NavBar.Tab', () => {
      const { $tab } = shallowTab({ disabled: true })

      expect($tab.find('.navtab-tab-disabled').length).toEqual(1)
    })

    it('render NavBar.Tab with custom className and custom style', () => {
      const { $tab } = shallowTab({ className: 'tab-test', style: { color: 'blue' } })
      const $li = $tab.find('.tab-test')

      expect($li.length).toEqual(1)
      expect($li.prop('style')).toHaveProperty('color', 'blue')
    })
  })
})
