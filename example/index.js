import ReactDOM from 'react-dom'
import React from 'react'

import UiComponents, { Common } from '@sat-mtl/ui-components'
import '@sat-mtl/ui-components/ui-components.css'

console.log(UiComponents)
console.log(Common)

const { Button, Icon } = Common

ReactDOM.render(
  <Button>
    <Icon type='call' />
  </Button>,
  document.getElementById('example')
)
