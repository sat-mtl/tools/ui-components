# @sat-mtl/ui-components

[![npm (tag)](https://img.shields.io/npm/v/@sat-mtl/ui-components/latest)](https://www.npmjs.com/package/@sat-valorisation/ui-components)
[![pipeline status](https://gitlab.com/sat-mtl/telepresence/ui-components/badges/develop/pipeline.svg)](https://gitlab.com/sat-mtl/telepresence/ui-components/-/commits/develop)
[![coverage report](https://gitlab.com/sat-mtl/telepresence/ui-components/badges/develop/coverage.svg)](https://gitlab.com/sat-mtl/telepresence/ui-components/-/commits/develop)

**UI Components** is a (WIP) library of React components meant to be used in telepresence projects currently developed by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/), a non-profit artistic entity based in Montreal, Canada.

See the [living style guide environment](https://sat-mtl.gitlab.io/telepresence/ui-components) in order to try, test and develop your own components.

## Install from NPM

```bash
>> npm i @sat-mtl/ui-components
```

## Bootstrap a development environment

A `nix-shell` script may be used to bootstrap a development environment. It uses [the Nix Package Manager](https://nixos.org/) and provides every tool required to contribute to this project.

```bash
>> nix-shell
Development environment with NodeJS v12.16.1 is ready!
```

## Usage

### Import JS components

All components are available inside the `UIComponents` object. Each of the *common* components are directly exported at root and each *scoped* component can be imported from their respective objects (ex: `Inputs`, `Context`...) For **CommonJS** imports, you must import each component separately.

```js
/* All components can be imported once */
import UIComponents from '@sat-mtl/ui-components'

const {
  Common: {
    Button,
    Icon,
    Menu,
    NavBar,
    Preview,
    Spinner
  },
  Context: {
    ThemeProvider,
    ThemeContext
  },
  Feedback: {
    Drawer,
    Modal
  },
  Inputs: {
    Checkbox,
    FormGroup,
    InputNumber,
    InputText,
    InputSelect,
    Select,
    Slider,
    Switch
  },
  Layout: {
    Cell,
    FlexBox,
    FlexColumn,
    FlexRow
  },
  Shared: {
    ColumnHead,
    ColumnSeparator,
    ConnectionCell,
    RowHead,
    SceneCell,
    ShmdataCell,
    VersionSection
  }
} = UIComponents

/* Each component is scoped */
import { Inputs } from '@sat-mtl/ui-components'

const { InputText } = Inputs
```

### Import SCSS styles

The current components are optimized with **Webpack**. It is highly recommended to use it with its [`style-loader`](https://github.com/webpack-contrib/style-loader). After installing these dependencies, use the embedded styles by importing them at the root of your development directory :

```js
import '@sat-mtl/ui-components/dist/ui-components.css'
```

### Use SCSS themes

All SCSS styles are loaded with a theme map (currently only one theme is being developed). Themes should be used with the `ThemeContext` and the `ThemeProvider` components.

```jsx
import React, { Component } from 'react'
import 'my-styles.scss'
import { Context } from '@sat-valorisation/ui-components'

const { ThemeProvider, ThemeContext } = Context

class MyComponent extends Component {
  static contextType = ThemeContext

  render () {
    return (
      <div className={`my-component-${this.context}`} />
    )
  }
}

ReactDOM.render((
  <ThemeProvider value='simon'>
    <MyComponent>
  </ThemeProvider>
), document.getElementById('my-component-example'))
```

The below JS script needs an HTML wrapper with a tag exposing the `my-component-example` ID. It also depends on the styles defined in `my-styles.scss`. These styles expose the `ui-components` themes by using the `themify` SCSS function:

```scss
@import '@ui-components/assets/themes/themify.utils.scss';
@import '@ui-components/assets/themes/theme.simon.scss';

$themes: (
  simon: $simon
);

@include themify('my-component', $themes) {
  & {
    font-family: themed('font-family');
    color: themed('text-primary');
    background-color: themed('background');
  }
}
```

### Use assets

The assets are imported into the `@sat/ui-components` package. You'll need to copy the assets into your build folder in order to use them. To easily resolve this issue, use the [`copy-webpack-plugin`](https://github.com/webpack-contrib/copy-webpack-plugin) in your **Webpack** configuration :

```js
const Copy = require('copy-webpack-plugin')

module.exports = {
  plugins: [
    new Copy([{
      from: 'assets',
      to: 'assets',
      context: 'node_modules/@sat-mtl/ui-components'
    }])
  ]
}
```

## Scale the element sizes with rem values

For components that are styled using `rem` units, a change in the font size of the root element will allow the resizing of those components in a way so as to preserve their shape. A root font size of 100% sets it to the default text size of 16px in browsers. If the user wants to set the root font size to 10px for example, they can add the following code to their html styles (In this case, 1rem would be equivalent to 10px):

```css
html {
  font-size: 62.5%;
}
```

## Contributing

Check out our [Contributing Guide](CONTRIBUTING.md) to get started!

A [nix shell](https://nixos.wiki/wiki/Development_environment_with_nix-shell) environment is set in order to automatize the development setup. You can use it by installing [nix](https://nixos.org/download.html) and by inputing `nix-shell` in your terminal at the root of the project.

## Versioning

[SemVer](http://semver.org/) is used for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/sat-mtl/telepresence/scenic/tags).

## Authors

See [here](AUTHORS.md).

## Licenses

This project is licensed under [the MIT License](LICENSE).

Otherwise the font families [Inter](https://rsms.me/inter/), [Exo2](https://www.ndiscovered.com/) and [Roboto](https://github.com/googlefonts/roboto) are bundled with the `ui-components`. According to their respective licence, each one is re-bundled :

+ [Exo2](assets/fonts/exo2/OFL.txt)
+ [Inter](assets/fonts/inter/LICENSE.txt)
+ [Roboto](assets/fonts/Roboto/Apache License.txt)

## Acknowledgments

This project was made possible by the [Société des Arts Technologiques [SAT]](http://sat.qc.ca/).
