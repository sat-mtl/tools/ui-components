with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "ui-components";

  nativeBuildInput = [
    binutils gcc gnumake openssl pkgconfig # common deps
  ];

  buildInputs = [
    git less vim # common tools
    nodejs-14_x # nodejs
    nodePackages.npm-check-updates
    entr
  ];

  shellHook = ''
    NODE_PATH="/tmp/node_modules";

    export PATH="$PATH:$NODE_PATH/bin";
    npm config set prefix "$NODE_PATH"

    if [ ! -e "$NODE_PATH/bin/standard" ]; then
       npm install standard --global
    fi

    echo "Development environment with NodeJS $(node --version) is ready!"
  '';
}
