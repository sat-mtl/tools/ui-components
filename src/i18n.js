import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

/**
 * Sets up the configuration of the i18n file for english and french languages
 * @param {Object} LocalesEn - An object listing all english translations
 * @param {Object} LocalesFr - An object listing all french translations
 * @param {?string} [DEFAULT_LANGUAGE='en'] - The default language to use when a chosen language is not available
 * @param {?string} [languageParam=null] - The language param that is used to changed the language, if required
 * @returns {Object} - The i18n configuration object
 */
const i18nConfig = (LocalesEn, LocalesFr, DEFAULT_LANGUAGE = 'en', languageParam = null) => {
  // To be added if we want the language to change according to a language param
  const language = languageParam ? new URLSearchParams(window.location?.search)?.get(languageParam) : null

  return (
    i18n
      .use(initReactI18next) // passes i18n down to react-i18next
      .init({
        resources: {
          en: {
            translation: {
              ...LocalesEn
            }
          },
          fr: {
            translation: {
              ...LocalesFr
            }
          }
        },
        lng: language || DEFAULT_LANGUAGE,
        fallbackLng: DEFAULT_LANGUAGE,

        keySeparator: '::',
        nsSeparator: ':::'
      })
  )
}

export default i18nConfig
