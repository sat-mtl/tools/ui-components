import Icon, { outlined, controls, filled } from './Icon'

export { outlined, controls, filled }
export default Icon
