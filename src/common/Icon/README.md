<!-- markdownlint-disable MD041 -->

## Outlined icons

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import Icon, { outlined } from './index';

const style = {
  width: '64px',
  height: '64px',
  margin: '4px'
}

const Icon64x64 = ({ type }) => (
  <div key={type} style={style}>
    <Icon type={type} />
  </div>
);

<FlexRow flexWrap='wrap'>
  {Object.keys(outlined).map(type =>
    <Icon64x64 key={type} type={type} />
  )}
</FlexRow>
```

## Control Icons (play, pause, stop)

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import Icon, { controls } from './index';

const style = {
  width: '32px',
  height: '32px',
  margin: '4px'
}

const Icon32x32 = ({ type }) => (
  <div key={type} style={style}>
    <Icon type={type} />
  </div>
);

<FlexRow flexWrap>
  {Object.keys(controls).map(type =>
    <Icon32x32 key={type} type={type} />
  )}
</FlexRow>
```

## Filled Colorable Icons

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'
import Icon, { filled } from './index';

const style = {
  display: 'flex',
  width: '64px',
  height: '64px',
  margin: '4px'
}

const colors = {
  'filled-critical': 'red',
  'filled-pause': 'blue',
  'filled-play': 'green',
  'filled-power': 'red',
  'filled-question': 'blue',
  'filled-stop': 'red',
  'filled-warning': 'orange'
}

const Icon64x64 = ({ type, color }) => (
  <div style={style}>
    <Icon type={type} color={color} />
  </div>
);

<FlexColumn>
  <FlexRow flexWrap>
    {Object.keys(filled).map(type =>
      <Icon64x64 key={type} type={type} />
    )}
  </FlexRow>
  <FlexRow flexWrap>
    {Object.keys(filled).map(type =>
      <Icon64x64 key={type} type={type} color={colors[type]} />
    )}
  </FlexRow>
</FlexColumn>
```

## Themable icons

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'
import Icon, { filled } from './index';

const style = {
  display: 'flex',
  width: '64px',
  height: '64px',
  margin: '4px'
}

const Icon64x64 = (props) => (
  <div style={style}>
    <Icon {...props} />
  </div>
);

<FlexRow flexWrap>
  <Icon64x64 type='call' withTheme />
  <Icon64x64 type='hangup' withTheme />
</FlexRow>
```
