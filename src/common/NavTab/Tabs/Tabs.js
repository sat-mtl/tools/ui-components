import React, { cloneElement, Component, Children } from 'react'
import PropTypes from 'prop-types'

import Bar from '../Bar'
import Tab from '../Tab'
import ExtraButton from '../ExtraButton'
import Panel from '../Panel'

/**
 * Tabs component is a Higher-Order Component used to display Tab, Panel and ExtraButton
 * @see Tab
 * @see Panel
 * @see ExtraButton
 */
class Tabs extends Component {
  tabMap = new Map()
  contentMap = new Map()

  overflowed = false
  $bar = React.createRef()

  static propTypes = {
    /** All components displayed into the Tab bar */
    children: PropTypes.any,
    /** Set an addon component after the Tab title */
    addonAfter: PropTypes.any,
    /** Set an addon component before the Tab title */
    addonBefore: PropTypes.any
  }

  state = {
    show: null
  }

  /**
   * Fills all component maps in order to display a Tab bar
   * @param {Tab|Panel|ExtraButton} child - Each child of the Tabs component
   * @param {number} index - Index of the processed component
   */
  fillComponentMaps (child, index) {
    const { tabMap, contentMap } = this
    const { show } = this.state
    const setShow = (tab) => this.setState({ show: tab })

    if (!child) return

    switch (child.type) {
      case Panel: {
        const { tab } = child.props

        contentMap.set(tab, child)
        tabMap.set(tab,
          <Tab key={tab} onClick={() => setShow(tab)} active={show === tab}>{tab}</Tab>
        )
        break
      }
      case Tab: {
        const { onClick } = child.props

        tabMap.set(index, cloneElement(child, {
          key: index,
          onClick: () => { setShow(index); onClick() },
          active: show === index
        }))
        break
      }

      case ExtraButton: {
        tabMap.set(index, child)
        break
      }

      default: {
        break
      }
    }
  }

  /** Clear all maps of the component */
  clearMaps () {
    [this.tabMap, this.contentMap].forEach(map => map.clear())
  }

  render () {
    const { show } = this.state
    const { children, addonAfter, addonBefore } = this.props
    const { tabMap, contentMap } = this

    this.clearMaps()
    Children.forEach(children, this.fillComponentMaps.bind(this))

    return (
      <>
        <Bar barRef={this.$bar} addonBefore={addonBefore} addonAfter={addonAfter}>
          {Array.from(tabMap.values())}
        </Bar>
        {contentMap.has(show) ? contentMap.get(show) : null}
      </>
    )
  }
}

export default Tabs
