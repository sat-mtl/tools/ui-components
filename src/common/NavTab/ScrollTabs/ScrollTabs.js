import React from 'react'
import PropTypes from 'prop-types'

import Tabs from '../Tabs'

/**
 * ScrollTabs add scrolling functionality on Tabs component
 * @extends Tabs
 */
class ScrollTabs extends Tabs {
  $bar = React.createRef()

  hasOverflow = false
  scrollLeft = 0

  static propTypes = {
    /** All components displayed into the Tab bar */
    children: PropTypes.any,
    /** Value of scrolling */
    scrollLeft: PropTypes.number,
    /**
     * Gets called when the Tab wrapper is overflowed
     * @param {boolean} hasOverflow - Flag which prevents if overflow is activated or not
     * @param {HTMLElement} $bar - HTML element of the Tab wrapper
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement HTMLElement doc} for usages
     */
    onOverflowChange: PropTypes.func,
    /**
     * Gets called when the scrollLeft value is changed
     * @param {number} scrollLeft - Value of the scrollLeft property
     * @param {HTMLElement} $bar - HTML element of the Tab wrapper
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement HTMLElement doc} for usages
     */
    onScrollChange: PropTypes.func
  }

  static defaultProps = {
    scrollLeft: 0,
    onOverflowChange: Function.prototype,
    onScrollChange: Function.prototype
  }

  /** Updates the overflow of the Tab wrapper */
  updateOverflow () {
    if (!this.$bar.current) return

    const $bar = this.$bar.current
    const hasOverflow = $bar.offsetWidth < $bar.scrollWidth

    if (hasOverflow !== !!this.hasOverflow) {
      this.props.onOverflowChange(hasOverflow, $bar)
      this.hasOverflow = hasOverflow
    }
  }

  /** Updates the scrollLeft value of the Tab wrapper */
  updateScrollLeft () {
    if (!this.$bar.current) return

    const $bar = this.$bar.current
    const { scrollLeft } = this.props

    $bar.scrollLeft = scrollLeft

    if ($bar.scrollLeft !== this.scrollLeft) {
      this.props.onScrollChange(scrollLeft, $bar)
      this.scrollLeft = $bar.scrollLeft
    }
  }

  componentDidUpdate () {
    this.updateOverflow()
    this.updateScrollLeft()
  }

  componentDidMount () {
    this.updateOverflow()
    this.updateScrollLeft()
  }
}

export default ScrollTabs
