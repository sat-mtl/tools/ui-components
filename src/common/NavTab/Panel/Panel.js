import React, { useContext } from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

/** Panel used to display content from a Tab */
function Panel ({ tab, children }) {
  const theme = useContext(ThemeContext)
  const cn = classNames('navtab-panel', `navtab-panel-${tab}`, `navtab-panel-${theme}`)

  return (
    <div className={cn}>{children}</div>
  )
}

Panel.propTypes = {
  /** Title of the associated Tab */
  tab: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  /** Content of the Panel */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired
}

export default Panel
