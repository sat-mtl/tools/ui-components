import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

export const types = [
  'item',
  'panel'
]

/**
 * Menu item used to display an element from a list, used with `Menu`
 * @see Menu
 */
function Item ({ type, children, onClick, onMouseDown, dataMenu }) {
  const theme = useContext(ThemeContext)
  const optionalProps = {}

  const className = classNames(
    'Item',
    `Item-${type}`,
    `Item-${type}-${theme}`
  )

  if (onClick) {
    optionalProps.onClick = onClick

    optionalProps.onMouseDown = (event) => {
      event.stopPropagation()
      event.preventDefault()
    }
  } else if (onMouseDown) {
    optionalProps.onMouseDown = onMouseDown
  }

  return (
    <li className={className} {...optionalProps} data-menu={dataMenu}>
      {children}
    </li>
  )
}

Item.propTypes = {
  /** Type of item */
  type: PropTypes.oneOf(types),
  /** Sets a function which is triggered when the item is clicked */
  onClick: PropTypes.func,
  /** Triggers the `onmousedown` event which is used to block the `onblur` event on parent menus */
  onMouseDown: PropTypes.func,
  /** Custom data-attribute for each menu item */
  dataMenu: PropTypes.string
}

Item.defaultProps = {
  onClick: null,
  onMouseDown: null,
  type: 'item'
}

export default Item
