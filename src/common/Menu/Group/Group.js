import React, { useContext } from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import Wrapper from '../Wrapper'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Menu group used to sort `Item` into categories.
 * @see Item
 */
function Group ({
  title,
  showGroup,
  accordion,
  collapsible,
  onClick,
  onMouseDown,
  children,
  width,
  addonBefore,
  addonAfter,
  dataMenu
}) {
  const theme = useContext(ThemeContext)

  const className = classNames(
    'Group',
    `Group-${theme}`,
    { CollapsibleGroup: collapsible || accordion },
    { AccordionGroup: accordion }
  )

  const childrenClassName = classNames(
    'GroupChildren',
    { CollapsedGroup: !showGroup }
  )

  return (
    <li className={className}>
      <span
        className='GroupTitle'
        onClick={onClick}
        onMouseDown={onMouseDown}
        data-menu={dataMenu}
      >
        {addonBefore && (
          <span className='Addon'>
            {addonBefore}
          </span>
        )}
        {title}
        {addonAfter && (
          <span className='Addon'>
            {addonAfter}
          </span>
        )}
      </span>
      <div className={childrenClassName}>
        <Wrapper
          expanded={showGroup}
          width={width}
          accordion={accordion}
        >
          {children}
        </Wrapper>
      </div>
    </li>
  )
}

Group.propTypes = {
  /** List of HTML elements displayed in a menu under the group */
  children: PropTypes.any,
  /** Title of the group */
  title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  /** Flag which make the groups collapsible and expandable */
  collapsible: PropTypes.bool,
  /** Flag which collapses the group when it is false */
  showGroup: PropTypes.bool,
  /** Flag which triggers an accordion animation when the menu is expanded */
  accordion: PropTypes.bool,
  /** Set a function which is triggered when the group is clicked */
  onClick: PropTypes.func,
  /** Triggers the `onmousedown` event which is used to prevent the `onblur` event on parent menus */
  onMouseDown: PropTypes.func,
  /** Set the width of the Group */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /** Set an addon component after the Group title */
  addonAfter: PropTypes.any,
  /** Set an addon component before the Group title */
  addonBefore: PropTypes.any,
  /** Binds a data attributes on the clickable element of the group */
  dataMenu: PropTypes.string
}

Group.defaultProps = {
  showGroup: false,
  collapsible: false,
  accordion: false,
  onClick: Function.prototype,
  onMouseDown: (event) => {
    event.stopPropagation()
    event.preventDefault()
  },
  width: 180
}

export default Group
