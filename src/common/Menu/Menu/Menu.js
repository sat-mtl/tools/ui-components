import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import Wrapper from '../Wrapper'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Hide the whole menu under a Button that looks like a Select input
 * @private
 * @selector `.MenuSelectButton`
 * @param {Function} [onClick] - Function triggered on click, usually used to expand itself.
 * @param {Function} [onMouseDown] - Add flexibility to debug conflict with the `mousedown` event
 * @param {external:react/Component} [addonBefore] - A component displayed before the title, usually an icon
 * @param {external:react/Component} [addonAfter] - A component displayed after the title, usually an icon
 * @param {string} [title] - The title of the menu
 * @param {string} [dataMenu] - Custom data-attribute for each menu collection
 * @returns {external:react/Component} A styled button
 */
export function SelectButton ({ onClick, onMouseDown, addonBefore, addonAfter, title, dataMenu }) {
  const theme = useContext(ThemeContext)
  const cn = classNames('SelectButton', `SelectButton-${theme}`)

  return (
    <button className={cn} onClick={onClick} onMouseDown={onMouseDown} data-menu={dataMenu}>
      {addonBefore && (
        <span className='Addon'>
          {addonBefore}
        </span>
      )}
      {title}
      {addonAfter && (
        <span className='Addon'>
          {addonAfter}
        </span>
      )}
    </button>
  )
}

/**
 * Display a `Wrapper` by hiding/showing a menu when it is clicked
 * @see Wrapper
 */
function Menu ({
  accordion,
  title,
  onClick,
  onMouseDown,
  onBlur,
  menuRef,
  expanded,
  addonBefore,
  addonAfter,
  children,
  width,
  dataMenu
}) {
  const theme = useContext(ThemeContext)
  const menuClass = classNames('Menu', `Menu-${theme}`)

  return (
    <div className={menuClass}>
      <SelectButton
        title={title}
        onClick={onClick}
        onMouseDown={onMouseDown}
        addonBefore={addonBefore}
        addonAfter={addonAfter}
        dataMenu={dataMenu}
      />
      <Wrapper
        top
        expanded={expanded}
        accordion={accordion}
        onBlur={onBlur}
        menuRef={menuRef}
        width={width}
      >
        {children}
      </Wrapper>
    </div>
  )
}

Menu.propTypes = {
  /** Title of the select */
  title: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  /** Sets a function which is triggered when the item is clicked */
  onClick: PropTypes.func,
  /** Sets a function which is triggered when the menu loses the focus */
  onBlur: PropTypes.func,
  /** Reference of the menu element, used to set focus after some actions */
  menuRef: PropTypes.any,
  /** Prevent the `onblur` event on parent menus (see default implementation) */
  onMouseDown: PropTypes.func,
  /** Flag which hides the `Menu.Wrapper` when it is false */
  expanded: PropTypes.bool,
  /** Children should be a `Menu.Wrapper` only */
  children: PropTypes.any,
  /** Set an addon component after the Menu title */
  addonAfter: PropTypes.any,
  /** Set an addon component before the Menu title */
  addonBefore: PropTypes.any,
  /** Set the width of the component */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /** Flag which triggers an accordion animation when the menu is expanded */
  accordion: PropTypes.bool,
  /** Custom data-attribute for each menu collection */
  dataMenu: PropTypes.string
}

Menu.defaultProps = {
  onClick: Function.prototype,
  onMouseDown: (event) => {
    event.stopPropagation()
    event.preventDefault()
  },
  expanded: false,
  addonAfter: null,
  addonBefore: null,
  width: 180,
  accordion: false
}

export default Menu
