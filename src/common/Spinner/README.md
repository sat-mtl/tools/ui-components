Minimal spinner built from [react-md-spinner](https://github.com/tsuyoshiwada/react-md-spinner) which is heavily inspired by Material Design principles.

```js
import ReactDOM from 'react-dom';

function launchSpinner () {
  ReactDOM.render(
    <Spinner />,
    document.getElementById('minimal-spinner')
  );

  setTimeout(() => {
    ReactDOM.unmountComponentAtNode(
      document.getElementById('minimal-spinner')
    );
  }, 3000);
}

<>
  <button onClick={launchSpinner}>Launch minimal Spinner</button>
  <div id='minimal-spinner' />
</>
```

This spinner is rendered with an optionnal message so it needs the initialisation of `i18next` in order to have a translation of the message.

```js
import ReactDOM from 'react-dom';

// import i18next from 'i18next';
// import { initReactI18next } from 'react-i18next';
// i18next.use(initReactI18next).init()

function launchSpinner () {
  ReactDOM.render(
    <Spinner message='Loading during 3 seconds...' />,
    document.getElementById('spinner-with-message')
  );

  setTimeout(() => {
    ReactDOM.unmountComponentAtNode(
      document.getElementById('spinner-with-message')
    );
  }, 3000);
}

<>
  <button onClick={launchSpinner}>Launch Spinner with message</button>
  <div id='spinner-with-message' />
</>
```
