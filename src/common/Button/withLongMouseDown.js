import React, { Component } from 'react'

/**
 * React's High Order Component that adds long mouse press capabilities
 * @param {Button} Wrapped - Button with long mouse press capabilities
 */
function withLongMouseDown (Wrapped) {
  return class extends Component {
    interval = 0

    startIntervalTimer (event) {
      const { intervalDelay, onClick } = this.props
      const callback = () => { onClick(event) }

      if (this.interval === 0) {
        this.interval = setInterval(callback, intervalDelay)
      }
    }

    stopIntervalTimer () {
      if (this.interval !== 0) {
        clearInterval(this.interval)
        this.interval = 0
      }
    }

    handleLongPress (event) {
      const { disabled, onClick } = this.props

      if (!onClick || disabled) return

      switch (event.type) {
        case 'mousedown':
          this.startIntervalTimer(event)
          break

        case 'mouseenter':
        case 'mouseup':
        case 'mouseleave':
          this.stopIntervalTimer()
          break
      }
    }

    render () {
      const mouseHandlers = {
        onMouseDown: this.handleLongPress.bind(this),
        onMouseEnter: this.handleLongPress.bind(this),
        onMouseUp: this.handleLongPress.bind(this),
        onMouseLeave: this.handleLongPress.bind(this)
      }

      return <Wrapped {...this.props} {...mouseHandlers} />
    }
  }
}

export default withLongMouseDown
