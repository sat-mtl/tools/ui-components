import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

export const types = [
  'primary',
  'secondary',
  'danger',
  'subtle',
  'heavy'
]

export const sizes = [
  'large',
  'normal',
  'small'
]

export const variants = [
  'contained',
  'outlined',
  'text'
]

export const shapes = [
  'rectangle',
  'circle',
  'square'
]

/**
 * Gets the class of the HTML button
 * @param {string} theme - Current theme of the component
 * @param {string} type - Type of the button
 * @param {string} className - Custom class of the button
 * @param {boolean} outlined - Flag if the button is outlined
 * @param {string} shape - Shape of the button
 * @param {boolean} disabled - Flag if the button is disabled
 * @param {string} size - The size of the button
 * @param {string} variant - The variant of the button
 * @returns {string} The concatenated class name
 */
function getButtonClass ({ theme, type, className, outlined, shape, disabled, size, variant }) {
  return classNames(
    `Button-${theme}`,
    { [`Button-${type}`]: !outlined },
    { [`Button-${type}-outlined`]: outlined },
    { [`Button-${size}`]: size },
    { [`Button-${variant}`]: variant },
    `Button-${type}`,
    { [`Button-${shape}`]: shape },
    { 'Button-disabled': disabled },
    className
  )
}

/** A button thats triggers everything */
function Button (props) {
  const theme = useContext(ThemeContext)
  const { disabled, children } = props

  const onButtonClick = (e) => {
    if (!disabled) props.onClick(e)
  }

  const mouseHandlers = {
    onClick: onButtonClick,
    onMouseDown: props.onMouseDown,
    onMouseEnter: props.onMouseEnter,
    onMouseUp: props.onMouseUp,
    onMouseLeave: props.onMouseLeave
  }

  const buttonProps = {
    className: getButtonClass({ ...props, theme }),
    disabled: disabled,
    type: 'button'
  }

  return (
    <button {...buttonProps} {...mouseHandlers}>
      {children}
    </button>
  )
}

Button.propTypes = {
  /** Class name for the HTML input */
  className: PropTypes.string,

  /** If `true`, the button is disabled */
  disabled: PropTypes.bool,

  /** Callback fired when the button is clicked */
  onClick: PropTypes.func,

  /** Shape of the button */
  shape: PropTypes.oneOf(shapes),

  /** Type of the button */
  type: PropTypes.oneOf(types),

  /** Variant of the button */
  variant: PropTypes.oneOf(variants),

  /** Size of the button */
  size: PropTypes.oneOf(sizes),

  /** Flag if the button is disabled */
  outlined: PropTypes.bool,

  /** @ignore Callback fired when the mouse is on the button */
  onMouseEnter: PropTypes.func,

  /** @ignore Callback fired when the mouse is pressing the button */
  onMouseDown: PropTypes.func,

  /** @ignore Callback fired when the mouse is releasing the button */
  onMouseUp: PropTypes.func,

  /** @ignore Callback fired when the mouse is leaving the button */
  onMouseLeave: PropTypes.func
}

Button.defaultProps = {
  className: '',
  disabled: false,
  onClick: Function.prototype,
  onMouseEnter: Function.prototype,
  onMouseDown: Function.prototype,
  onMouseUp: Function.prototype,
  onMouseLeave: Function.prototype,
  shape: undefined,
  type: 'primary',
  outlined: false,
  variant: 'contained',
  size: undefined
}

export default Button
