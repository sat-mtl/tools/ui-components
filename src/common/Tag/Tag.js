import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { createUseStyles } from 'react-jss'
import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'
import FlexRow from '~/layout/FlexRow'

const useStyles = createUseStyles({
  color: {
    backgroundColor: ({ color }) => `${color}`
  },
  clickable: {
    cursor: 'pointer'
  }
})

/**
 * A Tag component is used to categorize data or highlight a specific status
 * @visibleName Tag
 */
function Tag ({ className, status, color, children, addonBefore, addonAfter, onClick }) {
  const theme = useContext(ThemeContext)
  const classes = useStyles({ color })

  const cn = classNames(
    'tag',
    `tag-${theme}`,
    { [`tag-${status}`]: !!status },
    { [classes.color]: !!color && !status },
    { [classes.clickable]: !!onClick },
    className
  )

  let $addonBefore, $addonAfter

  if (addonBefore) {
    $addonBefore = (
      <div className='addon-before'>
        {addonBefore}
      </div>
    )
  }

  if (addonAfter) {
    $addonAfter = (
      <div className='addon-after'>
        {addonAfter}
      </div>
    )
  }

  return (
    <div className={cn} onClick={onClick || Function.prototype}>
      <FlexRow alignItems='center'>
        {$addonBefore}
        {children}
        {$addonAfter}
      </FlexRow>
    </div>
  )
}

Tag.propTypes = {
  /** Status of the tag. Its color overrides the `color` property */
  status: PropTypes.oneOf(
    Object.values(StatusEnum)
  ),
  /** Background color of the tag */
  color: PropTypes.string,
  /** Custom class name of the tag */
  className: PropTypes.string,
  /** Adds a custom element before the label */
  addonBefore: PropTypes.node,
  /** Adds a custom element after the label */
  addonAfter: PropTypes.node,
  /** Adds an `onClick` handler on the Tag */
  onClick: PropTypes.func
}

Tag.defaultProps = {
  className: ''
}

export default Tag
