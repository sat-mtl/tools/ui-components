import React, { createContext, Component } from 'react'
import PropTypes from 'prop-types'

/** Define default application theme */
export const ThemeContext = createContext('simon')

export const defaultTheme = 'simon'
export const availableThemes = ['ubald', 'legacy', 'simon', 'gabrielle']

/** Provide theme by fetching all theme configs */
class ThemeProvider extends Component {
  static propTypes = {
    value: PropTypes.oneOf(availableThemes),
    children: PropTypes.node,
    isDev: PropTypes.bool
  }

  static defaultProps = {
    value: defaultTheme,
    isDev: process.env.NODE_ENV === 'development'
  }

  fetchThemeFromURL (urlQuery) {
    const urlParameters = new URLSearchParams(urlQuery)
    return urlParameters.get('theme')
  }

  render () {
    let currentTheme = defaultTheme
    let { value, children, isDev } = this.props

    if (isDev) {
      value = this.fetchThemeFromURL(document.location.search) || value
    }

    if (availableThemes.includes(value)) {
      currentTheme = value
    }

    return (
      <ThemeContext.Provider value={currentTheme}>
        {children}
      </ThemeContext.Provider>
    )
  }
}

export default ThemeProvider
