import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

import Icon from '~/common/Icon'
import Cell from '~/layout/Cell'

function ConnectionCell ({
  onClick,
  armed,
  connected,
  disabled,
  selected,
  width = 70,
  height = 50,
  padding = 20
}) {
  const theme = useContext(ThemeContext)
  const [hovered, hover] = useState(false)

  const onCellClick = (e) => { onClick(e, armed, connected) }
  const cellProps = { width, height, padding, disabled }

  const armedOrHovered = (!hovered && armed) || (hovered && !armed)
  const armedAndHovered = hovered && armed

  const cellCn = classNames(
    'connection-cell',
    `connection-cell-${theme}`,
    { armed: armed },
    { selected: selected },
    { danger: armedAndHovered },
    { connected: connected }
  )

  const hoverProps = {
    onMouseEnter: () => { hover(true) },
    onMouseLeave: () => { hover(false) }
  }

  let $connection = null

  if (armedOrHovered) {
    $connection = (
      <Icon type='flux' color='#fff' />
    )
  } else if (armedAndHovered) {
    $connection = (
      <Icon type='close' color='#fff' />
    )
  }

  return (
    <Cell {...cellProps} {...hoverProps} className={cellCn} onClick={onCellClick}>
      {$connection}
    </Cell>
  )
}

ConnectionCell.propTypes = {
  ...Cell.propTypes,
  /** Disables/Enables the connection */
  armed: PropTypes.bool,
  connected: PropTypes.bool,
  selected: PropTypes.bool
}

ConnectionCell.defaultProps = {
  armed: false,
  connected: false,
  selected: false,
  onClick: () => {}
}

export default ConnectionCell
