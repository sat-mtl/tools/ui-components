import ConnectionCell from './ConnectionCell'

import './styles.common.scss'
import './styles.theme.scss'

export default ConnectionCell
