# Matrix Connection Box

```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'

import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'

import ConnectionCell from './index';

const titleStyle = {
  display: 'flex',
  fontSize: '12px',
  justifyContent: 'center'
};

<ThemeProvider value='simon'>
  <FlexRow columnGap={2}>
    <FlexColumn rowGap={2}>
      <div style={titleStyle}>Normal</div>
      <ConnectionCell />
    </FlexColumn>
    <FlexColumn rowGap={2}>
      <div style={titleStyle}>Connected</div>
      <ConnectionCell connected armed />
    </FlexColumn>
    <FlexColumn rowGap={2}>
      <div style={titleStyle}>Armed</div>
      <ConnectionCell armed />
    </FlexColumn>
    <FlexColumn rowGap={2}>
      <div style={titleStyle}>Disabled</div>
      <ConnectionCell disabled />
    </FlexColumn>
    <FlexColumn rowGap={2}>
      <div style={titleStyle}>Selected</div>
      <ConnectionCell selected />
    </FlexColumn>
  </FlexRow>
</ThemeProvider>
```
