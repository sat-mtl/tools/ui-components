import React, { useContext } from 'react'
import classNames from 'classnames'
import { ThemeContext } from '~/contexts/Theme'
import './VersionSection.scss'

/**
 * A link component that opens page in a new tab
 * @param {string} href - URL of the link
 * @param {node} children - Content of the link
 * @returns {external:react/Component} A tweaked HTML link
 */
function Link ({ href, children }) {
  return (
    <a target='_blank' rel='noopener noreferrer' href={href}>
      {children}
    </a>
  )
}

/**
 * @classdesc Model that displays a help resource
 * @memberof models
 */
class Resource {
  /**
   * Instantiates a new resource model
   * @param {string} [title] - Title of the resource
   * @param {string} [url] - URL of the resource
   * @param {string} [icon] - Icon of the resource
   */
  constructor (title, url, icon) {
    this.title = title
    this.url = url
    this.icon = icon
  }
}

/**
 * Renders the section for the software version and hash commit of an application
 * @param {string} repoLink - A link to the gitlab repository of the project
 * @param {Object} NpmPackage - A json object representing the json package of the app
 * @selector `#VersionSection`
 * @returns {external:react/Component} The software version section
 */
const VersionSection = ({ repoLink, NpmPackage, title = 'Software version' }) => {
  const theme = useContext(ThemeContext)

  const rootCn = classNames(
    'version-section',
    `version-section-${theme}`
  )

  const DEV_GIT = new Resource('source code', repoLink)
  const DEV_COMMIT = new Resource(process.env.GIT_SHORT_HASH, `${DEV_GIT.url}/-/commit/${process.env.GIT_SHORT_HASH}`)

  const $commit = (
    <Link href={DEV_COMMIT.url}>
      {DEV_COMMIT.title}
    </Link>
  )

  return (
    <section id='VersionSection' className={rootCn}>
      <h4>
        <span className='SectionIcon' role='img' aria-label='bookmark'>
          🔖
        </span>
        {title}
      </h4>
      <p id='ProjectVersion'>{NpmPackage.name}: {NpmPackage.version} ({$commit})</p>
    </section>
  )
}

export default VersionSection
