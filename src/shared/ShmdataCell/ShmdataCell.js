import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

import Cell from '~/layout/Cell'

const ICON_MARGIN_FIXES = ['data']

/** ShmdataCell shows the shmdata produced by a quiddity */
function ShmdataCell ({ onClick, selected, disabled, icon, src, alt, width = 70, height = 50, padding = 20 }) {
  const theme = useContext(ThemeContext)
  const common = { width, height, padding, disabled, onClick }

  common.className = classNames(
    'shmdata-cell',
    `shmdata-cell-${theme}`,
    { selected: !!selected },
    { disabled: !!disabled }
  )

  if (src) {
    common.padding = 0
  } else if (icon && ICON_MARGIN_FIXES.includes(icon.props.type)) {
    common.padding = 5
  }

  return (
    <Cell {...common}>
      {!!disabled || !src ? null : <img src={src} alt={alt} />}
      {src || !icon ? null : icon}
    </Cell>
  )
}

ShmdataCell.propTypes = {
  /** Disables/Enables the shmdata cell */
  disabled: PropTypes.bool,
  /** Select the shmdata cell */
  selected: PropTypes.bool,
  /** Representative icon of the shmdata */
  icon: PropTypes.node,
  /** Source URL of the shmdata thumbnail */
  src: PropTypes.string,
  /** Alternative text rendered if the thumbnail cannot be displayed */
  alt: PropTypes.string,
  /** Callback triggered when the shmdata is clicked */
  onClick: PropTypes.func
}

ShmdataCell.defaultProps = {
  src: null,
  alt: 'no',
  disabled: false,
  onClick: () => {}
}

export default ShmdataCell
