The RowHead is used in the Scenic matrix. It is used to display data for a series of items.

```js
import React, { useState } from 'react';

import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn'
import RowHead from './index';

function Sources () {
  return (
    <FlexColumn>
      <RowHead
        title='Moniteur Vidéo'
        subtitle='Normal'
      />
      <RowHead
        status='active'
        title='Sortie audio'
        subtitle='Started'
      />
      <RowHead
        title='Moniteur Vidéo'
        disabled
        status='danger'
        title='!@#$%&*'
        subtitle='Disabled'
      />
      <RowHead
        selected
        status='busy'
        title='Sortie NDI'
        subtitle='Selected'
      />
      <RowHead
        status='focus'
        title='AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
        subtitle='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
      />
    </FlexColumn>
  )
}

const containerStyle = {
  display: 'flex',
  flexDirection: 'column-reverse',
  width: 'fit-content',
  paddingRight: '100px'
};

<>
  <div style={{ ...containerStyle, backgroundColor: '#121212' }}>
    <ThemeProvider value='simon'>
      <Sources />
    </ThemeProvider>
  </div>
</>
```
