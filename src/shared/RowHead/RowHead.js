import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

/** RowHead displays a quiddity as a row header */
function RowHead ({ title, subtitle, status, selected, disabled, hovered, onClick }) {
  const theme = useContext(ThemeContext)

  const onHeadClick = (event) => {
    if (disabled) return
    onClick(event)
  }

  const statusCn = classNames(
    'status',
    { [`status-${status}`]: !!status }
  )

  const labelCn = classNames(
    'label',
    { selected: !!selected },
    { disabled: !!disabled }
  )

  const rootCn = classNames(
    'row-head',
    `row-head-${theme}`,
    { selected: !!selected },
    { disabled: !!disabled }
  )

  return (
    <div className={rootCn} onClick={onHeadClick}>
      <div className={statusCn} />
      <div className={labelCn}>
        <span className='title'>{title}</span>
        <span className='subtitle'>{subtitle}</span>
      </div>
    </div>
  )
}

RowHead.propTypes = {
  /** User-defined name of the source */
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /** Quiddity ID of the source */
  subtitle: PropTypes.string.isRequired,
  /** Indicates a specific status using a colored band */
  status: PropTypes.oneOf(Object.values(StatusEnum)),
  /** Flag which notifies if the source is selected */
  selected: PropTypes.bool,
  /** Flag which notifies if the source is disabled */
  disabled: PropTypes.bool,
  /** Flag which notifies if the source is hovered */
  hovered: PropTypes.bool,
  /** Triggered when the user clicks on the component */
  onClick: PropTypes.func
}

RowHead.defaultProps = {
  selected: false,
  disabled: false,
  onClick: Function.prototype
}

export default RowHead
