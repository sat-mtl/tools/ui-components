import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

/** ColumnHead displays a quiddity as a reader */
function ColumnHead ({ title, subtitle, status, selected, disabled, onClick }) {
  const theme = useContext(ThemeContext)

  const onHeadClick = (event) => {
    if (disabled) return
    onClick(event)
  }

  const rootCn = classNames(
    'column-head',
    `column-head-${theme}`,
    { selected: !!selected },
    { disabled: !!disabled }
  )

  const topCn = classNames(
    'shape',
    'top',
    'label',
    { selected: !!selected },
    { disabled: !!disabled }
  )

  const bottomCn = classNames(
    'bottom',
    { selected: !!selected },
    { disabled: !!disabled }
  )

  const statusCn = classNames(
    'status',
    { [`status-${status}`]: !!status },
    { disabled: !!disabled }
  )

  return (
    <div className={rootCn}>
      <div className={bottomCn} onClick={onHeadClick}>
        <div className={statusCn} />
      </div>
      <div className={topCn} onClick={onHeadClick}>
        <span className='title'>{title}</span>
        <span className='subtitle'>{subtitle}</span>
      </div>
    </div>
  )
}

ColumnHead.propTypes = {
  /** User-defined name of the destination */
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /** Quiddity ID of the destination */
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /** Indicates a specific status using a colored band */
  status: PropTypes.oneOf(Object.values(StatusEnum)),
  /** Flag which notifies if the destination is selected */
  selected: PropTypes.bool,
  /** Flag which notifies if the destination is disabled */
  disabled: PropTypes.bool,
  /** Triggered when the user clicks on the component */
  onClick: PropTypes.func
}

ColumnHead.defaultProps = {
  selected: false,
  disabled: false,
  onClick: Function.prototype
}

export default ColumnHead
