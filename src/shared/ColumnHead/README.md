# Matrix Column Head

```js
import React, { useState } from 'react';

import ThemeProvider from '~/contexts/Theme';
import FlexRow from '~/layout/FlexRow'
import ColumnHead from './index';

function Destinations () {
  return (
    <FlexRow>
      <ColumnHead
        title='Moniteur Vidéo'
        subtitle='Normal'
      />
      <ColumnHead
        status='active'
        title='Sortie audio'
        subtitle='Started'
      />
      <ColumnHead
        disabled
        status='danger'
        title='!@#$%&*'
        subtitle='Disabled'
      />
      <ColumnHead
        selected
        status='busy'
        title='Sortie NDI'
        subtitle='Selected'
      />
      <ColumnHead
        status='focus'
        title='AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
        subtitle='aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
      />
    </FlexRow>
  )
};

const containerStyle = {
  height: '140px',
  display: 'flex',
  flexDirection: 'column-reverse',
  width: 'fit-content',
  paddingRight: '100px'
};

<>
  <h3>Simon style</h3>
  <div style={{ ...containerStyle, backgroundColor: '#121212' }}>
    <ThemeProvider value='simon'>
      <Destinations />
    </ThemeProvider>
  </div>
</>
```
