import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import Cell from '~/layout/Cell'
import { ThemeContext } from '~/contexts/Theme'

/** SceneCell represents a Scene as a minimalistic toggle button */
function SceneCell ({ children, onClick, active, disabled, ...common }) {
  const theme = useContext(ThemeContext)
  const [hovered, hover] = useState(false)

  const onSceneClick = (e) => {
    if (!disabled) onClick(e, active)
  }

  common.className = classNames(
    'scene-cell',
    `scene-cell-${theme}`,
    { active: !!active }
  )

  return (
    <Cell
      {...common}
      disabled={disabled}
      selected={hovered}
      onClick={onSceneClick}
      onMouseEnter={() => { hover(true) }}
      onMouseLeave={() => { hover(false) }}
    >
      <div className='scene-name'>
        {children}
      </div>
    </Cell>
  )
}

SceneCell.propTypes = {
  ...Cell.propTypes,
  /** Status of the scene */
  active: PropTypes.bool,
  /** Callback triggered when the cell is clicked */
  onClick: PropTypes.func
}

SceneCell.defaultProps = {
  active: false,
  onClick: () => {}
}

export default SceneCell
