import common from '~/common'
import contexts from '~/contexts'
import feedback from '~/feedback'
import inputs from '~/inputs'
import layout from '~/layout'
import shared from '~/shared'
import i18n from '~/i18n'

export const Common = common
export const Context = contexts
export const Feedback = feedback
export const Inputs = inputs
export const Layout = layout
export const Shared = shared
export const i18nConfig = i18n

export default {
  Common,
  Context,
  Feedback,
  Inputs,
  Layout,
  Shared,
  i18nConfig
}
