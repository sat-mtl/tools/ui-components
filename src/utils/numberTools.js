/**
 * Normalizes a numeric value to [0, 1]
 * @param {number} min - Minimal value allowed
 * @param {number} max - Maximal value allowed
 * @param {number} value - Current slider value
 */
export function normalize (min, max, value) {
  if (max === min) {
    throw new Error('Minimal and maximal parameter cannot be equals!')
  } else {
    return (value - min) / (max - min)
  }
}

/**
 * Denormalizes a numeric value from [0, 1]
 * @param {number} min - Minimal value allowed
 * @param {number} max - Maximal value allowed
 * @param {number} normalized - Current normalized slider value
 */
export function denormalize (min, max, normalized) {
  if (normalized < 0 || normalized > 1) {
    throw new Error('Input value must be normalized!')
  } else {
    return normalized * (max - min) + min
  }
}

/**
 * Transforms a numeric value into a percentage
 * @param {number} min - Minimal value allowed
 * @param {number} max - Maximal value allowed
 * @param {number} value - Current slider value
 */
export function convertToPercentage (min, max, value) {
  return normalize(min, max, value) * 100
}

/**
 * Gets the number of digits from a step
 * @param {number} step - The step of number selectors
 */
export function getDigits (step) {
  const absStep = Math.abs(step)
  const log = absStep > 1 ? 0 : Math.abs(Math.log10(absStep))

  return Math.round(log)
}

/**
 * Parses a number with requested decimals
 * @param {number|string} value - Value to parse
 * @param {number} digits - The number of digits to appear after the decimal point
 */
export function parseDouble (value, digits) {
  const str = Number.parseFloat(value).toFixed(digits)

  if (digits === 0) {
    return Number.parseInt(str)
  } else {
    return Number.parseFloat(str)
  }
}

/**
 * Default number validation
 * @param {number} min - Minimal value allowed
 * @param {number} max - Maximal value allowed
 * @param {number} oldVal - Current slider value
 * @param {number} newVal - User inputed value
 * @param {number} digits - The number of digits to appear after the decimal point
 */
export function validateNumber (min, max, oldVal, newVal, digits = 0) {
  let value = parseDouble(newVal, digits)

  if (value > max) {
    value = max
  } else if (value < min) {
    value = min
  } else if (Number.isNaN(value)) {
    value = oldVal
  }

  return value
}
