import React from 'react'
import FlexBox from './FlexBox'

/** Wrapper around column-based CSS flexbox */
const FlexColumn = ({ children, ...props }) => (
  <FlexBox flexDirection='column' {...props}>
    {children}
  </FlexBox>
)

export default FlexColumn
