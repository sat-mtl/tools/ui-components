import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  backdrop: {
    position: 'fixed',
    backgroundColor: 'black',
    opacity: 0.2
  },
  raised: {
    zIndex: props => props.zIndex
  },
  fullScreen: {
    top: 0,
    left: 0,
    height: '100%',
    width: '100%'
  }
})

function Backdrop ({ onClick, zIndex }) {
  const classes = useStyles({ zIndex })

  const cn = classNames(
    'backdrop',
    classes.backdrop,
    classes.fullScreen,
    classes.raised
  )

  return (
    <div className={cn} onClick={onClick} />
  )
}

Backdrop.propTypes = {
  /** Function triggered when the backdrop is clicked */
  onClick: PropTypes.func,
  /** Define the z-index CSS property of the drawer */
  zIndex: PropTypes.number
}

Backdrop.defaultProps = {
  onClick: Function.prototype,
  zIndex: 100
}

export default Backdrop
