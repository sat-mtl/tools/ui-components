import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

import Backdrop from '~/layout/Backdrop'

const useStyles = createUseStyles({
  screen: {
    position: 'fixed',
    backgroundColor: 'black',

    bottom: props => props.bottom,
    left: props => props.left,

    minWidth: '160px',
    minHeight: '120px',

    width: props => props.width,
    height: props => props.height,

    zIndex: props => props.zIndex || 101
  }
})

/** Launches a preview from a picture with a backdrop in order to highlight it */
function Preview ({
  width,
  height,
  bottom,
  left,
  zIndex,
  src,
  alt,
  onBackdropClick,
  withBackdrop
}) {
  const classes = useStyles({ width, height, bottom, left, zIndex })

  const previewStyle = {
    width: width,
    height: height
  }

  const screenCn = classNames(
    'screen',
    classes.screen
  )

  const $backdrop = (
    <Backdrop onClick={onBackdropClick} />
  )

  return (
    <div className='preview'>
      <div className={screenCn} style={previewStyle}>
        <img src={src} alt={alt} />
      </div>
      {withBackdrop && $backdrop}
    </div>
  )
}

Preview.propTypes = {
  /** Source which will be displayed into the preview */
  src: PropTypes.string,
  /** Alternative text displayed if there is no source */
  alt: PropTypes.string,
  /** Bottom position of the preview screen */
  bottom: PropTypes.string,
  /** Left position of the preview screen */
  left: PropTypes.string,
  /** Width of the preview */
  width: PropTypes.number,
  /** Height of the preview */
  height: PropTypes.number,
  /** Flag to add a backdrop under the preview */
  withBackdrop: PropTypes.bool,
  /** Function triggered when the backdrop is clicked */
  onBackdropClick: PropTypes.func,
  /** Define the z-index CSS property of the modal */
  zIndex: PropTypes.number
}

Preview.defaultProps = {
  alt: 'No content...',
  onBackdropClick: Function.prototype,
  left: '82px',
  bottom: '12px',
  withBackdrop: true
}

export default Preview
