# Scenic preview

The preview display a minimalist window with a picture as content, this one is displayed with default dimensions.

```js
import ReactDOM from 'react-dom';
import React, { useState } from 'react';
import Button from '~/common/Button';

function Example () {
  const [isVisible, togglePreview] = useState(false)
  let $preview = null

  if (isVisible) {
    $preview = (
      <Preview
        src='http://placehold.it/160x120&text=image1'
        onBackdropClick={() => togglePreview(false)}
      />
    )
  }

  return (
    <>
      <Button onClick={() => togglePreview(true)}>
        Show Preview
      </Button>
      {$preview}
    </>
  )
}

<Example />
```
