<!-- markdownlint-disable MD041 -->

The modal has three structural properties :

* `header` is specifying the header of the modal, it is encapsulated with the HTML tag `<header/>`.
* `footer` is specifying the footer of the modal, it is encapsulated with the HTML tag `<footer/>`.
* The modal children elements are the content of the drawer and are encapsulated with the HTML tag `<main/>`.

```jsx
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'
import Button from '~/common/Button'
import Modal from './index';
import FlexRow from '~/layout/FlexRow';

const [visible, toggle] = useState(false);
const onClick = () => toggle(!visible);

<FlexRow>
  <Button onClick={onClick}>Trigger Modal</Button>
  <Modal
    header='Confirm you want to perform action'
    onBackdropClick={onClick}
    visible={visible}
    footer={
      <FlexRow justifyContent='space-evenly'>
        <Button type='secondary' onClick={onClick}>Cancel</Button>
        <Button type='primary' onClick={onClick}>Continue</Button>
      </FlexRow>
    }
  >
    You are about to perform a critical action. This will destroy everything.
    <br/><br/>
    Do you wish to proceed? You are about to perform a critical action.
    This will destroy everything. You are about to perform a critical action.
  </Modal>
</FlexRow>
```

The `width` and the `zIndex` values can be changed in order tothe modal styling.
Be careful, changing the `zIndex` value will also change the backdrop `zIndex`.

```jsx
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'
import Button from '~/common/Button'
import Modal from './index';
import FlexRow from '~/layout/FlexRow';

const [visible, toggle] = useState(false);
const onClick = () => toggle(!visible);

<FlexRow>
  <Button onClick={onClick}>Trigger Modal</Button>
  <Modal
    onBackdropClick={onClick}
    visible={visible}
    width={800}
  >
    <p>
      Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa egestas mollis varius;
      dignissim elementum. Mollis tincidunt mattis hendrerit dolor eros enim, nisi ligula ornare.
      Hendrerit parturient habitant pharetra rutrum gravida porttitor eros feugiat. Mollis elit
      sodales taciti duis praesent id. Consequat urna vitae morbi nunc congue.
    </p>
  </Modal>
</FlexRow>
```
