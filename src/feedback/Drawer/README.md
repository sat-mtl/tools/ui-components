<!-- markdownlint-disable MD041 -->

First, the drawer has three structural properties :

* `header` will specify the header content of the drawer, it is encapsulated with the HTML tag `<header/>`.
* `footer` is specifying the footer of the drawer, it is encapsulated with the HTML tag `<footer/>`.
* The drawer children elements are the content of the drawer and are encapsulated with the HTML tag `<main/>`.

In addition, the property `width` can be used to change the width of the drawer.

```jsx
import React, { useState } from 'react';
import FlexRow from '~/layout/FlexRow';
import Button from '~/common/Button';
import Drawer from './index';

const [visible, toggle] = useState(false);
const onClick = () => toggle(!visible);

<FlexRow justifyContent='space-evenly'>
  <Button onClick={onClick}>
    Show drawer
  </Button>
  <Drawer
    side='left'
    header={<p>I am a the <b>header</b></p>}
    footer={<p>I am a the <b>footer</b></p>}
    visible={visible}
    onBackdropClick={onClick}
  >
    <p>
      Lorem ipsum odor amet, consectetuer adipiscing elit. Ac purus in massa egestas mollis varius;
      dignissim elementum. Mollis tincidunt mattis hendrerit dolor eros enim, nisi ligula ornare.
      Hendrerit parturient habitant pharetra rutrum gravida porttitor eros feugiat. Mollis elit
      sodales taciti duis praesent id. Consequat urna vitae morbi nunc congue.
    </p>
    <p>
      Non etiam tempor id arcu magna ante eget. Nec per posuere cubilia cras porttitor condimentum
      orci suscipit. Leo maecenas in tristique, himenaeos elementum placerat. Taciti rutrum nostra,
      eget cursus velit ultricies. Quam molestie tellus himenaeos cubilia congue vivamus ultricies.
      Interdum praesent ut penatibus fames eros ad consectetur sed.
    </p>
  </Drawer>
</FlexRow>
```

The drawer component can be displayed on the sides `right` and `left` with `side` property.

```jsx
import React, { useState } from 'react';
import FlexRow from '~/layout/FlexRow';
import Button from '~/common/Button';
import Drawer from './index';

const [rightVisible, toggleRight] = useState(false);
const onClickRight = () => toggleRight(!rightVisible);

const [leftVisible, toggleLeft] = useState(false);
const onClickLeft = () => toggleLeft(!leftVisible);

<FlexRow justifyContent='space-evenly'>
  <section>
    <Button onClick={onClickLeft}>
      Left
    </Button>
    <Drawer
      side='left'
      header='I am on the left'
      visible={leftVisible}
      onBackdropClick={onClickLeft}
    />
  </section>
  <section>
    <Button onClick={onClickRight}>
      Right
    </Button>
    <Drawer
      side='right'
      header='I am on the right'
      visible={rightVisible}
      onBackdropClick={onClickRight}
    />
  </section>
</FlexRow>
```

Also, the backdrop can be removed by setting the `withBackdrop` property to false.

```jsx
import React, { useState } from 'react';
import FlexRow from '~/layout/FlexRow';
import Button from '~/common/Button';
import Drawer from './index';

const [withoutVisible, toggleWithout] = useState(false);
const onClickWithout = () => {
  if (withVisible) onClickWith()
  toggleWithout(!withoutVisible)
};

const [withVisible, toggleWith] = useState(false);
const onClickWith = () => {
  if (withoutVisible) onClickWithout()
  toggleWith(!withVisible)
};

<FlexRow justifyContent='space-evenly'>
  <section>
    <Button onClick={onClickWith}>
      With backdrop
    </Button>
    <Drawer
      side='right'
      header='With backdrop'
      visible={withVisible}
      onBackdropClick={onClickWith}
    >
      You can click on the backdrop to close the drawer.
    </Drawer>
  </section>
  <section>
    <Button onClick={onClickWithout}>
      Without backdrop
    </Button>
    <Drawer
      side='right'
      header='Without backdrop'
      visible={withoutVisible}
      withBackdrop={false}
    >
    You must reset this drawer visibility by clicking again on the trigger,
    or by clicking <a style={{ textDecoration: 'underline', cursor: 'pointer' }} onClick={onClickWithout}>here</a>.
    </Drawer>
  </section>
</FlexRow>
```
