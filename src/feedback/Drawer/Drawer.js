import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'
import Backdrop from '~/layout/Backdrop'

import { createUseStyles } from 'react-jss'
import { CSSTransition } from 'react-transition-group'

const useStyles = createUseStyles({
  drawer: {
    display: props => props && props.visible ? 'block' : 'none',
    width: props => `${props.width}px`
  },
  raised: {
    zIndex: props => props.zIndex || 100
  },
  fullScreen: {
    top: 0,
    left: 0,
    height: '100%',
    width: '100%'
  },
  side: {
    right: props => props.side === 'right' ? 0 : 'unset',
    left: props => props.side === 'left' ? 0 : 'unset'
  }
})

/**
 * Drawer component that displays extra menu on the side
 * @visibleName Drawer
 */
function Drawer ({ children, header, footer, withBackdrop, onBackdropClick, ...props }) {
  const { visible, id } = props
  const classes = useStyles(props)
  const theme = useContext(ThemeContext)

  const cn = classNames(
    'Drawer',
    `Drawer-${theme}`,
    classes.drawer,
    classes.raised,
    classes.side
  )

  const wrapperCn = classNames(
    'drawer-wrapper',
    classes.raised
  )

  const $backdrop = (
    <div className={classes.fullScreen}>
      <Backdrop onClick={onBackdropClick} zIndex={props.zIndex} />
    </div>
  )

  return (
    <CSSTransition in={visible} timeout={400} classNames='Drawer'>
      <div id={id} className={cn}>
        {withBackdrop && $backdrop}
        <div className={wrapperCn}>
          {header && <header>{header}</header>}
          <main>{children}</main>
          {footer && <footer>{footer}</footer>}
        </div>
      </div>
    </CSSTransition>
  )
}

Drawer.propTypes = {
  /** ID of the drawer */
  id: PropTypes.string,
  /** Body content of the drawer */
  children: PropTypes.node,
  /** Header content of the drawer */
  header: PropTypes.node,
  /** Flag to add a backdrop under the drawer */
  withBackdrop: PropTypes.bool,
  /** Function triggered when the drawer backdrop is clicked */
  onBackdropClick: PropTypes.func,
  /** Define if the drawer is visible */
  visible: PropTypes.bool,
  /** Define the z-index CSS property of the drawer */
  zIndex: PropTypes.number,
  /** Define the width of the drawer */
  width: PropTypes.number,
  /** Define the side of the drawer */
  side: PropTypes.oneOf(['left', 'right'])
}

Drawer.defaultProps = {
  children: null,
  header: null,
  width: 375,
  withBackdrop: true,
  onBackdropClick: Function.prototype,
  visible: false,
  zIndex: 100,
  side: 'right'
}

export default Drawer
