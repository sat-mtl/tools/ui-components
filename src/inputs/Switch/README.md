<!-- markdownlint-disable MD041 -->

```js
import React, { useState } from 'react';
import Switch from './index'
import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';
import FlexRow from '~/layout/FlexRow';

function DynamicInput ({ ...props }) {
  const [checked, toggleSwitch] = useState(false);

  return (
    <Switch
      checked={checked}
      onChange={(toggle) => { toggleSwitch(toggle) }}
      {...props}
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <FlexRow columnGap={16}>
      <ThemeProvider value='simon'>
        <FlexColumn rowGap={8}>
          <DynamicInput />
          <Switch disabled />
        </FlexColumn>
        <FlexColumn rowGap={8}>
          <DynamicInput shape='rectangle' />
          <Switch disabled shape='rectangle' />
        </FlexColumn>
      </ThemeProvider>
      <ThemeProvider value='legacy'>
        <FlexColumn rowGap={8}>
          <DynamicInput shape='rectangle' />
          <Switch disabled shape='rectangle' />
        </FlexColumn>
      </ThemeProvider>
    </FlexRow>
  </div>
</div>
```
