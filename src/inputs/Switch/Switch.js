import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Gets the class name of the root component
 * @private
 * @param {boolean} disabled - Flags a disabled component
 * @param {string} theme - Current theme of the component
 * @param {string} shape - Current shape of the component
 * @param {boolean} checked - Flags a checked component
 * @param {string} status - Current status of the component
 * @returns {string} The concatenated class name
 */
function getRootClass ({ disabled, theme, shape, checked, status }) {
  return classNames(
    'Switch',
    `Switch-${theme}`,
    `Switch-${shape}`,
    `Switch-${status}`,
    { 'Switch-checked': checked },
    { 'Switch-unchecked': !checked },
    { 'Switch-disabled': disabled }
  )
}

/**
 * Gets the class name of the Switch button
 * @private
 * @param {string} status - Checked status of the switch
 * @returns {string} The concatenated class name
 */
function getButtonClass ({ status }) {
  return classNames(
    'SwitchButton',
    { [`SwitchButton-${status}`]: status }
  )
}

/**
 * Switch lets users toggle values
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function Switch (props) {
  const theme = useContext(ThemeContext)
  const { disabled, onChange, checked, checkedLabel, uncheckedLabel } = props

  const rootClass = getRootClass({ ...props, theme })

  const buttonProps = {
    type: 'button',
    className: getButtonClass(props),
    onClick: (e) => !disabled && onChange(!checked, e),
    disabled: disabled,
    tabIndex: 0
  }

  const fieldProps = {
    type: 'checkbox',
    disabled: disabled,
    checked: checked,
    style: { display: 'none' },
    onChange: Function.prototype
  }

  return (
    <div className={rootClass}>
      <input {...fieldProps} />
      <button {...buttonProps}>
        <label className='SwitchLabel'>
          {checked ? checkedLabel : uncheckedLabel}
        </label>
      </button>
    </div>
  )
}

Switch.propTypes = {
  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Checked status of the component */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /**
   * Callback fired when the value is changed
   * @param {bool} checked - Value of the switch state
   * @param {Object} event - HTML event fired when the switch is clicked
   */
  onChange: PropTypes.func,

  /** Determine whether the Switch is checked */
  checked: PropTypes.bool,

  /** Content to be shown when the state is unchecked */
  uncheckedLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Content to be shown when the state is checked */
  checkedLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Determine the shape of the switch */
  shape: PropTypes.oneOf(['rectangle', 'circle'])
}

Switch.defaultProps = {
  disabled: false,
  onChange: Function.prototype,
  checked: false,
  checkedLabel: 'ON',
  uncheckedLabel: 'OFF',
  shape: 'circle'
}

export default Switch
