import React from 'react'
import classNames from 'classnames'

import AriaMenuButton from 'react-aria-menubutton'
import ScrollBar from 'react-customscroll'

/**
 * Gets the class name of the menu
 * @private
 * @param {string} size - Size of the menu
 * @param {string} place - Place where the menu is displayed
 * @param {string} theme - Current theme of the menu
 * @returns {string} The concatenated class name
 */
function getMenuClass ({ size, place, theme }) {
  return classNames(
    'SelectMenu',
    `SelectMenu-${theme}`,
    `SelectMenu-${size}`,
    `SelectMenu-${place}`
  )
}

/**
 * SelectMenu's Option encapsulates all select options
 * @private
 * @param {string} label - Label of the option
 * @param {string} value - Value of the option
 * @returns {React.PureComponent} An option component
 */
export function Option ({ label, value }) {
  return (
    <li key={value}>
      <AriaMenuButton.MenuItem className='SelectOption'>
        {label}
      </AriaMenuButton.MenuItem>
    </li>
  )
}

/**
 * Menu of the Select component
 * @private
 */
function SelectMenu (props) {
  const { options } = props
  const menuClass = getMenuClass(props)

  return (
    <AriaMenuButton.Menu className={menuClass}>
      <div className='SelectMask' />
      <ul>
        <ScrollBar>
          {options.map(opt => <Option key={opt.label} {...opt} />)}
        </ScrollBar>
      </ul>
    </AriaMenuButton.Menu>
  )
}

export default SelectMenu
