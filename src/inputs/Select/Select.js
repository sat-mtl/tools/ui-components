import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

/**
 * A React component that will help you build accessible menu buttons
 * by providing keyboard interactions and ARIA attributes aligned
 * with the WAI-ARIA Menu Button Design Pattern.
 * @note Inner mechanisms such as `onSelection` are not testable, but it is tested internally
 * @note 07/01/2020 The library doesn't seem well-maintained
 */
import AriaMenuButton from 'react-aria-menubutton'

import SelectMenu from './SelectMenu'
import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Gets the class name of the root component
 * @private
 * @param {string} theme - Current theme of the component
 * @param {string} place - Place where the Select menu is expanded
 * @param {string} size - Size of the component
 * @param {string} status - Current status of the component
 * @param {boolean} disabled - Flag a disabled component
 * @returns {string} The concatenated class name
 */
function getRootClass ({ theme, place, size, status, disabled }) {
  return classNames(
    'Select',
    `Select-${theme}`,
    `Select-${place}`,
    `Select-${size}`,
    { 'Select-disabled': disabled },
    { [`Select-${status}`]: status }
  )
}

/**
 * Gets the class name of the field component
 * @private
 * @param {string} label - Label to display in the field
 * @returns {string} The concatenated class name
 */
function getFieldClass ({ selected: { label } }) {
  return classNames(
    'SelectField',
    { SelectPlaceholder: !label }
  )
}

/** Select lets users select information from a list of options */
function Select (props) {
  const theme = useContext(ThemeContext)

  const { selected, placeholder, disabled, onSelection } = props
  const { label } = selected

  const rootClass = getRootClass({ ...props, theme })
  const fieldClass = getFieldClass(props)

  return (
    <AriaMenuButton.Wrapper className={rootClass} onSelection={onSelection}>
      <AriaMenuButton.Button className='SelectButton' disabled={disabled}>
        <div className={fieldClass}>
          {label || placeholder}
        </div>
        <div className='SelectIcon' />
      </AriaMenuButton.Button>
      {!disabled && <SelectMenu theme={theme} {...props} />}
    </AriaMenuButton.Wrapper>
  )
}

Select.propTypes = {
  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Give a hint to the user of what can be entered in the control */
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),

  /** Place where the Select menu is displayed */
  place: PropTypes.oneOf(['top', 'bottom']),

  /** Current status of the component */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** The function triggered on user's selection */
  onSelection: PropTypes.func,

  /**
   * The options of the select
   * @see See [MobX observable array](https://mobx.js.org/refguide/array.html)
   */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
    })
  ),

  /** Size of the component */
  size: PropTypes.oneOf(['normal', 'tiny']),

  /**
   * The selected option
   * @see See [MobX observable object](https://mobx.js.org/refguide/object.html)
   */
  selected: PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
  })
}

Select.defaultProps = {
  disabled: false,
  placeholder: 'Select an item...',
  place: 'bottom',
  id: undefined,
  status: StatusEnum.FOCUS,
  selected: {},
  size: 'normal',
  onSelection: Function.prototype,
  options: []
}

export default Select
