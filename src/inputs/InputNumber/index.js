import InputNumber from './InputNumber'

import withInputValidation from './withInputValidation.js'

import './InputNumber.scss'

export default withInputValidation(InputNumber)
export { withInputValidation }
