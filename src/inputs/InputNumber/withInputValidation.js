import React, { Component } from 'react'
import { validateNumber, getDigits } from '~/utils/numberTools'

/**
 * React's High Order Component that adds validation capabilities to InputNumber
 * @param {InputNumber} Wrapped - InputNumber with input validation
 */
function withInputValidation (Wrapped) {
  return class extends Component {
    /**
     * Handles all validation triggers which are:
     *  + When the user presses **Enter**
     *  + When the input looses focus
     * @param {SyntheticEvent} event - React’s Event System
     */
    handleValidation (event) {
      if (!event.target) return

      const { min, max, value, step, onChange } = this.props
      const $input = event.target
      let validated = value

      if ($input.value) {
        validated = validateNumber(min, max, value, $input.value, getDigits(step))
      }

      if (validated !== value) {
        onChange(validated)
      } else {
        // if there are no changes, reset the input value
        // fix some input number bugs: see https://bugzilla.mozilla.org/show_bug.cgi?id=1398528
        $input.value = value
      }
    }

    /**
     * Handles the `onkeypress` event and validates if the pressed key is **Enter**
     * @param {SyntheticEvent} event - React’s Event System
     */
    handleKeyDown (event) {
      if (!event.which) return

      if (event.which === 13) {
        this.handleValidation(event)
      }
    }

    render () {
      const validationHandlers = {
        onKeyDown: this.handleKeyDown.bind(this),
        onBlur: this.handleValidation.bind(this)
      }

      return <Wrapped {...this.props} {...validationHandlers} />
    }
  }
}

export default withInputValidation
