<!-- markdownlint-disable MD041 -->

The Field should group inputs under a [fieldset HTML element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/fieldset).

It passes the `status` and `disabled` properties to all its child inputs.

Also, it's a good practice to encapsulate the field component into a `<form>` element in order to monitor the input statements.

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react'

import Field from './index'

import ThemeProvider from '~/contexts/Theme'
import FlexColumn from '~/layout/FlexColumn'
import Input from '~/inputs'

const options = [{
  label: 'Item 1',
  value: 'item1'
}, {
  label: 'Item 2',
  value: 'item2'
}, {
  label: 'Item 3',
  value: 'item3'
}, {
  label: 'Item 4',
  value: 'item4'
}]

const [text, setText] = useState(undefined)
const [number, setNumber] = useState(0)
const [selected, setSelection] = useState(undefined)
const [checked, toggleSwitch] = useState(false)

const onSubmit = (event) => {
  event.preventDefault()

  console.log({
    msg: 'All field are submitted',
    fields: {
      text: text,
      number: number,
      selected: selected,
      checked: checked
    }
  })
}

;<form onSubmit={onSubmit}>
  <FlexColumn>
    <Field
      title='I am an InputText'
      description='Warning: My purpose is inputing only textual values!'
      status='busy'>
      <Input.Text
        value={text}
        onChange={(e) => { setText(e.target.value) }} />
    </Field>
    <Field
      title='I am an InputNumber'
      description='Error: My purpose is inputing only numbers!'
      status='danger'>
      <Input.Number
        value={number}
        onChange={(v) => { setNumber(v) }} />
    </Field>
    <Field
      title='I am a Select'
      description='My purpose is selecting a value among choices!'
      status='focus'>
      <Input.Select
        options={options}
        selected={selected}
        onSelection={(value) => {
          const item = options.find(item => item.label == value)
          setSelection({ label: item.label, value: item.value })
        }} />
    </Field>
    <Field
      title='I am a Switch'
      disabled
      description='Sorry, I am disabled'>
      <Input.Switch
        checked={checked}
        onChange={(toggle) => { toggleSwitch(toggle) }} />
    </Field>
  </FlexColumn>
</form>
```
