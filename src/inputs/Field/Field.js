import React, { useContext, cloneElement, Children } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Gets the class name of the field component
 * @private
 * @param {string} theme - Current theme of the component
 * @param {string} status - Current status of the component
 * @param {boolean} disabled - Flags if the component is disabled
 * @returns {string} The concatenated class name
 */
function getFieldClass ({ theme, status, disabled }) {
  return classNames(
    'Field',
    `Field-${theme}`,
    { [`Field-${status}`]: !!status },
    { 'Field-disabled': disabled }
  )
}

/** Field is used to display any user input */
function Field ({ children, title, description, disabled, status }) {
  const theme = useContext(ThemeContext)
  const $children = Children.map(children, child => cloneElement(child, { status, disabled }))

  return (
    <fieldset className={getFieldClass({ theme, status, disabled })}>
      {!!title && <label className='FieldTitle'>{title}</label>}
      {$children}
      {!!description && <label className='FieldDescription'>{description}</label>}
    </fieldset>
  )
}

Field.propTypes = {
  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Propagate the status to the attached input */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** Title text for the input */
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Description of the input */
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
}

Field.defaultProps = {
  disabled: false
}

export default Field
