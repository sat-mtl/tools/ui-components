import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { createUseStyles } from 'react-jss'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

export const sizes = ['tiny', 'normal', 'large', 'xlarge']
export const shapes = ['square', 'circle']

/**
 * Gets the class name of the root component
 * @private
 * @param {string} theme - Current theme of the component
 * @param {string} size - Size of the checkbox
 * @param {string} shape - Shape of the checkbox
 * @param {boolean} disabled - Flags a disabled checkbox
 * @param {string} status - Status of the checkbox
 * @returns {string} The concatenated class name
 * @todo Remove the status logic that is deprecated because it is hard to maintain compatibility
 */
function getRootClass ({ theme, size, shape, disabled, status, classes, color }) {
  return classNames(
    'Checkbox',
    `Checkbox-${theme}`,
    `Checkbox-${size}`,
    `Checkbox-${shape}`,
    { 'Checkbox-disabled': disabled },
    { [`Checkbox-${status}`]: status && !color },
    { [classes.checkbox]: color }
  )
}

/**
 * Gets the properties of the input component
 * @private
 * @param {boolean} disabled - Flags a disabled checkbox
 * @param {string} label - Label of the checkbox
 * @param {boolean} checked - Flags a checked component
 * @returns {object} The properties of the <input /> component
 */
function getFieldProps ({ disabled, label, checked }) {
  return {
    disabled: disabled,
    type: 'checkbox',
    value: label,
    checked: checked,
    onChange: Function.prototype
  }
}

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({

  styledCheckbox: {
    borderColor: props => props.color
  },
  checkbox: {
    backgroundColor: '#121212',
    '&:checked + span': {
      borderColor: props => props.color
    }
  }
})

/**
 * Checkbox is a Controlled Component which lets users enter and edit text
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function Checkbox (props) {
  const classes = useStyles(props)
  const theme = useContext(ThemeContext)
  const { disabled, checked, children, onChange } = props

  const onCheck = (e) => {
    if (!disabled) onChange(e, !checked)
  }

  const rootClass = getRootClass({ ...props, theme, classes })
  const fieldProps = getFieldProps(props)

  return (
    <div className={rootClass} onClick={onCheck}>
      <input {...fieldProps} />
      <span className={classes.styledCheckbox}>
        {checked ? children : null}
      </span>
    </div>
  )
}

Checkbox.propTypes = {
  /** If true, the input is checked */
  checked: PropTypes.bool,

  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Callback fired when the value is changed */
  onChange: PropTypes.func,

  /** Label of the Checkbox */
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Size of the Checkbox */
  size: PropTypes.oneOf(sizes),

  /** Shape of the Checkbox */
  shape: PropTypes.oneOf(shapes),

  /** Status of the Checkbox */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** Customized color of the Checkbox */
  color: PropTypes.string
}

Checkbox.defaultProps = {
  checked: false,
  disabled: false,
  onChange: Function.prototype,
  label: '',
  size: 'normal',
  shape: 'square',
  status: StatusEnum.FOCUS
}

export default Checkbox
