import React from 'react'

/**
 * The rail component detects the mouse slides
 * @private
 * @param {boolean} disabled - Flags if the component is disabled
 * @param {function} onSlide - Callback triggered when the mouse is sliding
 * @param {node} children - The tracked children
 */
function Rail ({ disabled, onSlide, children }) {
  const [isMouseInside, setInsideState] = React.useState(false)
  const [isMouseDown, setDownState] = React.useState(false)

  let handlers = {}

  if (!disabled) {
    handlers = {
      onMouseMove: (e) => {
        isMouseDown && isMouseInside && onSlide(e)
      },
      onMouseLeave: (e) => {
        setInsideState(false)
        isMouseDown && setDownState(false)
        isMouseDown && onSlide(e)
      },
      onMouseEnter: () => setInsideState(true),
      onMouseDown: (e) => {
        setDownState(true)
        isMouseInside && onSlide(e)
      },
      onMouseUp: () => setDownState(false)
    }
  }

  return (
    <div className='SliderRail' {...handlers}>
      {children}
    </div>
  )
}

export default Rail
