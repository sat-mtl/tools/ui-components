import Checkbox from './Checkbox'
import InputNumber from './InputNumber'
import InputText from './InputText'
import InputPassword from './InputPassword'
import InputSelect from './InputSelect'
import Select from './Select'
import Switch from './Switch'
import Slider from './Slider'

import Field from './Field'

// InputNumber is prefixed with Input in order to
// avoid conflicts with the built-in Number js module.

export default {
  Checkbox,
  Field,
  InputText,
  InputNumber,
  Number: InputNumber,
  InputPassword,
  InputSelect,
  Select,
  Slider,
  Switch,
  Text: InputText
}
