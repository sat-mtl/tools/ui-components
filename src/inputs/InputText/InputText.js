import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import StatusEnum from '~/common/StatusEnum'
import FlexRow from '~/layout/FlexRow'
import { ThemeContext } from '~/contexts/Theme'
import FlexColumn from '../../layout/FlexColumn'

export const sizes = [
  'large',
  'normal',
  'small'
]

/**
 * InputText's Addon encapsulates all addons
 * @private
 * @param {string|React.node} addon - An addon before or after the input text
 * @returns {React.PureComponent} An addon component
 */
function Addon ({ addon }) {
  return (
    <span className='InputTextAddon'>
      {addon}
    </span>
  )
}

/**
 * Gets the class names of the root component
 * @private
 * @param {string} theme - Current theme
 * @param {boolean} disabled - Flag if the field is disabled
 * @param {string} status - Current status of the field
 * @returns {string}  All concatenated class names
 */
function getRootClass ({ theme, disabled, status, rootClassName, size }) {
  return classNames(
    'InputText',
    `InputText-${size}`,
    `InputText-${theme}`,
    `InputText-${size}-${theme}`,
    { 'InputText-disabled': disabled },
    { [`InputText-${status}`]: status },
    rootClassName
  )
}

/**
 * Gets the class names of the <input /> field
 * @private
 * @param {string} extraClassName - Extra class name of the field
 * @returns {string} All concatenated class names
 */
function getFieldClass (extraClassName) {
  return classNames(
    'InputTextField',
    extraClassName
  )
}

/**
 * Gets the properties of the root component
 * @private
 * @param {string} className - Class name of the root component
 * @param {string} theme - Current theme of the component
 * @param {boolean} disabled - Flags if the component is disabled
 * @param {string} status - Current status of the component
 * @returns {Object} All properties of the root component
 */
function getRootProps ({ className, theme, disabled, status }) {
  return {
    className: className,
    alignItems: 'center',
    rowGap: 8
  }
}

/**
 * Gets the properties of the <input /> field
 * @private
 * @param {boolean} disabled - Flag if the component is disabled
 * @param {string} className - Extra class name of the field
 * @param {string} placeholder - Placeholder of the field
 * @param {string} name - Name of the field
 * @param {function} onBlur - Function triggered when the component is blurred
 * @param {function} onInput - Function triggered when the user inputs values
 * @param {function} onClick - Function triggered when the user clicks on the input
 * @param {function} onFocus - Function triggered when the user focuses the input
 * @param {function} onKeyPress - Function triggered when the user presses a key
 * @param {string} value - Text value of the input
 * @returns {Object} All the properties of the field
 */
function getFieldProps ({ disabled, className, placeholder, name, onBlur, onInput, onClick, onFocus, onKeyPress, value, size }) {
  return {
    disabled: disabled,
    className: className,
    type: 'text',
    placeholder: placeholder,
    name: name,
    value: value,
    onBlur: onBlur,
    onChange: onInput,
    onClick: onClick,
    onFocus: onFocus,
    onKeyPress: onKeyPress
  }
}

/**
 * InputText is a Controlled Component which lets users enter and edit text
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function InputText (props) {
  const theme = useContext(ThemeContext)

  const { disabled, onChange, onPressEnter } = props
  const { addonAfter, addonBefore } = props
  const { inputProps, title } = props

  const onInput = (e) => {
    if (!disabled) onChange(e)
  }

  const onKeyPress = (e) => {
    if (!disabled && e.key === 'Enter') onPressEnter(e)
  }

  const rootClass = getRootClass({ ...props, theme })
  const fieldClass = getFieldClass(props.className)

  const rootProps = getRootProps({ ...props, theme, className: rootClass })
  const fieldProps = getFieldProps({ ...props, onInput, onKeyPress, theme, className: fieldClass })

  return (
    <FlexColumn {...rootProps}>
      {title && props.size === 'large' && <label>{title}</label>}
      <FlexRow>
        {addonBefore && <Addon addon={addonBefore} />}
        <input {...fieldProps} {...inputProps} />
        {addonAfter && <Addon addon={addonAfter} />}
      </FlexRow>
    </FlexColumn>
  )
}

InputText.propTypes = {
  /** Root class name of the component */
  rootClassName: PropTypes.string,

  /** Class name for the HTML input */
  className: PropTypes.string,

  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Status of the input */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** Callback fired when the input is blurred */
  onBlur: PropTypes.func,

  /** Callback fired when the value is changed */
  onChange: PropTypes.func,

  /** Callback fired when the input is clicked */
  onClick: PropTypes.func,

  /** Callback fired when the input is focused */
  onFocus: PropTypes.func,

  /** Callback fired when the user presses enter in the input */
  onPressEnter: PropTypes.func,

  /** Short hint displayed in the empty input */
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /**
   * Add a name for the input (useful for Bitwarden autofill)
   * @see [Using custom fields]{@link https://bitwarden.com/help/article/custom-fields/}
   */
  name: PropTypes.string,

  /** Value of the input */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Set an addon component after the input */
  addonAfter: PropTypes.any,

  /** Set an addon component before the input */
  addonBefore: PropTypes.any,

  /** Extra properties for the input component */
  inputProps: PropTypes.object,

  /** The size of the input component */
  size: PropTypes.oneOf(sizes),

  /** The title of the input component */
  title: PropTypes.string
}

InputText.defaultProps = {
  disabled: false,
  onBlur: Function.prototype,
  onChange: Function.prototype,
  onClick: Function.prototype,
  onPressEnter: (event) => {
    event.target.blur()
  },
  onFocus: (event) => {
    event.target.select()
  },
  placeholder: 'Input a value...',
  inputProps: {},
  size: 'normal',
  title: 'label'
}

export default InputText

export { Addon, getRootClass, getFieldClass }
