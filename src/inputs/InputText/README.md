<!-- markdownlint-disable MD041 -->

```js
import React, { useState } from 'react';
import InputText from './index'
import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

function DynamicInput ({ value }) {
  const [input, setInput] = useState(value);

  return (
    <InputText
      placeholder='A value is required'
      value={input}
      onChange={(e) => { setInput(e.target.value) }}
      size='normal'
    />
  )
}

function ErrorInput () {
  const [value, setValue] = useState(undefined);
  const hasError = !value

  return (
    <InputText
      placeholder='A value is required'
      value={value}
      hasError={!value}
      onChange={(e) => { setValue(e.target.value) }}
      size='normal'
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <ThemeProvider value='simon'>
      <FlexColumn rowGap={8}>
        <DynamicInput value={'a'.repeat(100)} />
        <ErrorInput />
        <InputText value='Disabled' disabled />
      </FlexColumn>
    </ThemeProvider>
  </div>
</div>
```

```js
import React, { useState } from 'react';
import InputText from './index'
import ThemeProvider from '~/contexts/Theme';

import FlexColumn from '~/layout/FlexColumn';
import Icon from '~/common/Icon';

function Input ({ value, addonBefore, addonAfter, status }) {
  const [input, setInput] = useState(value);

  return (
    <InputText
      placeholder='A value is required'
      value={input}
      onChange={(e) => { setInput(e.target.value) }}
      addonBefore={addonBefore}
      addonAfter={addonAfter}
      status={status}
      size='normal'
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <FlexColumn rowGap={8}>
      <InputText status="danger" addonAfter={<Icon type='alert' />} />
      <InputText status="busy" addonBefore={<Icon type='alert' />} />
    </FlexColumn>
  </div>
</div>
```
## WIP: Gabrielle style

```js
import React, { useState } from 'react';
import InputText from './index'
import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

function DynamicInput ({ value }) {
  const [input, setInput] = useState(value);

  return (
    <InputText
      placeholder='A value is required'
      value={input}
      onChange={(e) => { setInput(e.target.value) }}
      size='large'
      title='label'
    />
  )
}

function ErrorInput () {
  const [value, setValue] = useState(undefined);
  const hasError = !value

  return (
    <InputText
      placeholder='A value is required'
      value={value}
      hasError={!value}
      onChange={(e) => { setValue(e.target.value) }}
      size='large'
      title='label'
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <ThemeProvider value='gabrielle'>
      <FlexColumn rowGap={8}>
        <DynamicInput value={'a'.repeat(100)} />
        <ErrorInput />
        <InputText value='Disabled' disabled />
      </FlexColumn>
    </ThemeProvider>
  </div>
</div>
```

```js
import React, { useState } from 'react';
import InputText from './index'
import ThemeProvider from '~/contexts/Theme';

import FlexColumn from '~/layout/FlexColumn';
import Icon from '~/common/Icon';

function Input ({ value, addonBefore, addonAfter, status }) {
  const [input, setInput] = useState(value);

  return (
    <InputText
      placeholder='A value is required'
      value={input}
      onChange={(e) => { setInput(e.target.value) }}
      addonBefore={addonBefore}
      addonAfter={addonAfter}
      status={status}
      size='large'
      title='label'
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
  <ThemeProvider value='gabrielle'>
    <FlexColumn rowGap={8}>
      <InputText status="danger" size='large' addonAfter={<Icon type='alert' />} />
      <InputText status="busy" size='large' addonBefore={<Icon type='alert' />} />
    </FlexColumn>
  </ThemeProvider>
  </div>
</div>
```
