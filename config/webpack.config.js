const path = require('path')
const { execSync } = require('child_process')

const MiniCssExtract = require('mini-css-extract-plugin')

const LIFECYCLE_EVENT = process.env.npm_lifecycle_event

const { EnvironmentPlugin } = require('webpack')

/**
 * Path to the root folder of the source code
 * @member {string} jsEntry
 */
const rootPath = path.resolve(__dirname, '..')

/**
 * Path to the JS entry point for the ui-components library
 * @see [Webpack entry configuration]{@link https://webpack.js.org/configuration/entry-context/#entry}
 * @member {string} jsEntry
 */
const jsEntry = path.resolve(rootPath, 'src/index.js')

/**
 * Path to the output folder for the ui-components library
 * @see [Webpack output configuration]{@link https://webpack.js.org/configuration/output/#outputfilename}
 * @member {string} entryFile
 */
const outputFolder = path.resolve(rootPath, 'dist')

/**
 * Defines the SVG loader into React components
 * @see [The SVG loader documentation]{@link https://react-svgr.com/docs/webpack/}
 * @member {object} A webpack loader
 */
const svgrLoader = {
  loader: '@svgr/webpack',
  options: {
    svgo: false
  }
}

const common = {
  entry: {
    'ui-components': jsEntry
  },
  output: {
    filename: '[name].js',
    path: outputFolder,
    library: {
      type: 'umd'
    }
  },
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
    'react-jss': 'react-jss',
    'react-aria-menubutton': 'react-aria-menubutton',
    'react-customscroll': 'react-customscroll',
    'react-md-spinner': 'react-md-spinner',
    'react-transition-group': 'react-transition-group',
    i18next: 'i18next',
    'react-i18next': 'react-i18next'
  },
  resolve: {
    alias: {
      '~': path.resolve(rootPath, 'src'),
      '@assets': path.resolve(rootPath, 'assets')
    }
  },
  plugins: [
    new MiniCssExtract({
      filename: 'ui-components.css'
    }),
    new EnvironmentPlugin({
      GIT_SHORT_HASH: execSync('git rev-parse --short HEAD', { encoding: 'utf8' }).trim()
    })
  ],
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: [MiniCssExtract.loader, 'css-loader', 'sass-loader']
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      type: 'asset/inline'
    }, {
      test: /\.svg$/,
      use: ['babel-loader', svgrLoader]
    }, {
      test: /\.svg$/,
      issuer: /\s?css$/,
      loader: 'url-loader'
    }]
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }
}

if (LIFECYCLE_EVENT === 'prepare') {
  module.exports = { ...common, mode: 'production' }
} else {
  module.exports = { ...common, mode: 'development' }
}
