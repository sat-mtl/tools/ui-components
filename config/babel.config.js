module.exports = {
  sourceMaps: true,
  presets: [
    '@babel/preset-react',
    ['@babel/preset-env', { targets: { node: 'current' } }]
  ],
  plugins: [
    ['@babel/plugin-proposal-class-properties', { loose: true }],
    ['@babel/plugin-proposal-private-property-in-object', { loose: true }],
    ['@babel/plugin-proposal-optional-chaining']
  ]
}
